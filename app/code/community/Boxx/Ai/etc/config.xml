<?xml version="1.0"?>
<!--
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * Boxx Ai Global Configuration
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author	   AIBOXX Solutions Pvt Ltd
 * @copyright Copyright (c) 2016-2018 AIBOXX Solutions Pvt Ltd (https://boxx.ai)
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @user 	   Boxx Team
*/
-->
<config>
    <modules>
        <Boxx_Ai>
            <version>1.0.1</version>
        </Boxx_Ai>
    </modules>
	<!-- Global Decleration-->
    <global>
    	<blocks>
            <ai>
                <class>Boxx_Ai_Block</class>
            </ai>
        </blocks>
		<!-- Global Model Decleration-->
        <models>
            <ai>
                <class>Boxx_Ai_Model</class>
                <resourceModel>boxx_ai_resource</resourceModel>
            </ai>
			<boxx_ai_resource>
				<class>Boxx_Ai_Model_Resource</class>
				<entities>		  
					<productsync>
						<table>boxx_productsync</table>
					</productsync>
					<ordersync>
                        <table>boxx_ordersync</table>
                    </ordersync>
				</entities>
			</boxx_ai_resource>
        </models>
		<!-- Global Helpers Decleration-->
        <helpers>
            <ai>
                <class>Boxx_Ai_Helper</class>
            </ai>
        </helpers>
		<!-- Global Events Decleration - All Observers -->
		<events>
			<!-- Observer called after product add/update -->
			<catalog_block_product_list_collection>
				<observers>
					<ai>
						  <type>model</type>
						  <class>ai/Observer_Recommendations</class>
						  <method>plprecommendations</method>
					</ai>
				</observers>
			</catalog_block_product_list_collection>	
			<!-- Observer called after product add/update -->
			<catalog_product_save_after>
				<observers>
					<ai>
						  <type>model</type>
						  <class>ai/Observer_Admin</class>
						  <method>productAddEditClick</method>
					</ai>
				</observers>
			</catalog_product_save_after>			
			<!-- Observer called after product status update -->
			<resource_get_tablename>
				<observers>
					<ai>
						  <class>ai/autoloader</class>
						  <method>addAutoloader</method>
					</ai>
				</observers>
			</resource_get_tablename>
			<!-- Observer called on produt delete mass -->
			<catalog_controller_product_delete>
				<observers>
					<ai>
						  <type>model</type>
						  <class>ai/Observer_Admin</class>
						  <method>productDelete</method>
					</ai>
				</observers>
			</catalog_controller_product_delete>		
			<!-- Observer called on produt delete -->
			<catalog_product_delete_after>
				<observers>
					<ai>
						  <type>model</type>
						  <class>ai/Observer_Admin</class>
						  <method>productDelete</method>
					</ai>
				</observers>
			</catalog_product_delete_after>
			<!-- Observer called on customer login -->
			<customer_login>
				<observers>
					<ai>
					   <type>singleton</type>
					   <class>ai/Observer_Frontend</class>
					   <method>customerLoginTrack</method>
				   </ai>
			   </observers>
		  </customer_login>
			<!-- Observer called on newsletter subscribtion -->
			<newsletter_subscriber_save_commit_after>
				<observers>
					<ai>
					   <type>singleton</type>
					   <class>ai/Observer_Frontend</class>
					   <method>newsletterSubscribeTrack</method>
				   </ai>
			   </observers>
		  </newsletter_subscriber_save_commit_after>
			<!-- Observer called on customer register -->
			<customer_register_success>
				<observers>
					<ai>
					   <type>singleton</type>
					   <class>ai/Observer_Frontend</class>
					   <method>customerRegisterTrack</method>
				   </ai>
			   </observers>
		  </customer_register_success>
			<!-- Observer called on customer register -->
			<customer_save_after>
				<observers>
					<ai>
					   <type>singleton</type>
					   <class>ai/Observer_Frontend</class>
					   <method>customerRegisterTrack</method>
				   </ai>
			   </observers>
		  </customer_save_after>
		  <!-- Observer called on save of plugin configuration settings -->
		  <admin_system_config_changed_section_ai>
				<observers>
					<ai>
					   <type>singleton</type>
					   <class>ai/Observer_Admin</class>
					   <method>adminConfigSave</method>
				   </ai>
			   </observers>
		  </admin_system_config_changed_section_ai>		  
		  <!-- Observer called to get the notification messages -->
		  <ai_notifications_before>
				<observers>
					<ai>
					   <type>singleton</type>
					   <class>ai/Observer_Admin</class>
					   <method>checkMessages</method>
				   </ai>
			   </observers>
		  </ai_notifications_before> 
		  <!-- Observer called on add to cart -->
		  <controller_action_predispatch_checkout_cart_add>
				<observers>
					<ai>
					   <type>singleton</type>
					   <class>ai/Observer_Frontend</class>
					   <method>pushAddtoCart</method>
				   </ai>
			   </observers>
		  </controller_action_predispatch_checkout_cart_add>
		  <!-- Observer called on remove from cart -->
		  <sales_quote_remove_item>
				<observers>
					<ai>
					   <type>singleton</type>
					   <class>ai/Observer_Frontend</class>
					   <method>pushRemovetoCart</method>
				   </ai>
			   </observers>
		  </sales_quote_remove_item>
		  <!-- Observer called on add to wishlist -->
		  <wishlist_add_product>
				<observers>
					<ai>
					   <type>singleton</type>
					   <class>ai/Observer_Frontend</class>
					   <method>pushWishlisttoCart</method>
				   </ai>
			   </observers>
		  </wishlist_add_product>
		  <!-- Observer called on remove from wishlist -->
		  <controller_action_predispatch_wishlist_index_remove>
				<observers>
					<ai>
					   <type>singleton</type>
					   <class>ai/Observer_Frontend</class>
					   <method>pushRemoveWishlisttoCart</method>
				   </ai>
			   </observers>
		  </controller_action_predispatch_wishlist_index_remove>
		  <!-- Observer called on checkout page -->
		  <controller_action_predispatch_checkout_onepage_index>
				<observers>
					<ai>
					   <type>singleton</type>
					   <class>ai/Observer_Frontend</class>
					   <method>pushCheckoutData</method>
				   </ai>
			   </observers>
		  </controller_action_predispatch_checkout_onepage_index>
		  <!-- Observer called after order placed -->
		  <sales_order_place_after>
				<observers>
					<ai>
					   <type>singleton</type>
					   <class>ai/Observer_Frontend</class>
					   <method>pushOrderData</method>
				   </ai>
			   </observers>
		  </sales_order_place_after>
		  <!-- Observer called after customer edit -->
		  <adminhtml_customer_save_after>
				<observers>
					<ai>
					   <type>singleton</type>
					   <class>ai/Observer_Admin</class>
					   <method>updateCustomerData</method>
				   </ai>
			   </observers>
		  </adminhtml_customer_save_after>
		  <!-- Observer called after customer delete -->
		  <customer_delete_after>
				<observers>
					<ai>
					   <type>singleton</type>
					   <class>ai/Observer_Admin</class>
					   <method>deleteCustomerData</method>
				   </ai>
			   </observers>
		  </customer_delete_after>
		  <!-- Observer called after bulk or single product attribute update -->
		  <!-- <catalog_product_attribute_update_after>
				<observers>
					<ai>
					   <type>singleton</type>
					   <class>ai/Observer_Admin</class>
					   <method>productAttributeUpdate</method>
				   </ai>
			   </observers>
		  </catalog_product_attribute_update_after> -->
		</events>	
		<!-- Global resource decleration -->		
        <resources>
            <ai_setup>
                <setup>
                    <module>Boxx_Ai</module>
					<class>Mage_Catalog_Model_Resource_Eav_Mysql4_Setup</class>
                </setup>
                <connection>
                    <use>core_setup</use>
                </connection>
            </ai_setup>
            <ai_write>
                <connection>
                    <use>core_write</use>
                </connection>
            </ai_write>
            <ai_read>
                <connection>
                    <use>core_read</use>
                </connection>
            </ai_read>
        </resources>
    </global>
	<!-- Frontend decleration -->		
    <frontend>
		<layout>
            <updates>
                <module>
                    <file>boxx_ai.xml</file>
                </module>
            </updates>
        </layout>
		<routers>
            <ai>
                <use>standard</use>
                <args>
                    <module>Boxx_Ai</module>
                    <frontName>boxxai</frontName>
                </args>
            </ai>
        </routers>
    </frontend>
	<!-- Admin decleration -->
	<admin>
        <routers>
            <adminhtml>
                <args>
                    <modules>
                        <boxx_ai before="Mage_Adminhtml">Boxx_Ai_Adminhtml</boxx_ai>
                    </modules>
                </args>
            </adminhtml>
        </routers>
    </admin>		
	<adminhtml>
		<layout>
            <updates>
                <module>
                    <file>boxx_ai.xml</file>
                </module>
            </updates>
        </layout>
		<!-- Admin Configuration Tab Placement -->		
		<acl>
			<resources>
				<all>
					<title>Allow Everything</title>
				</all>
				<admin>
					<children>
						<system>
							<children>
								<config>
									<children>
										<ai>
											<title>Boxx Ai - All</title>
										</ai>
									</children>
								</config>
							</children>
						</system>
					</children>
				</admin>
			</resources>
		</acl>
	</adminhtml>
	<crontab>
        <jobs>
            <boxx_product_sync_cron>
			    <schedule>
                    <!-- <cron_expr>*/2 * * * *</cron_expr> -->
                    <config_path>ai/ai_product_sync/boxx_product_cron_schedule_expr</config_path>
                </schedule>
                <run>
                    <model>ai/datasync::run</model>
                </run>
            </boxx_product_sync_cron>
            <boxx_order_sync_cron>
			    <schedule>
                    <!--<cron_expr>*/2 * * * *</cron_expr>-->
                    <config_path>ai/ai_order_sync/boxx_order_cron_schedule_expr</config_path>
                </schedule>
                <run>
                    <model>ai/datasync::runOrderSyncJob</model>
                </run>
            </boxx_order_sync_cron>
        </jobs>
    </crontab>
</config> 
