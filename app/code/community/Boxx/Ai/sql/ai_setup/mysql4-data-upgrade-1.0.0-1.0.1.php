<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * Boxx Ai Setup Installer
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @copyright Copyright (c) 2016-2018 AIBOXX Solutions Pvt Ltd (https://boxx.ai)
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @user       Boxx Team
*/
try{
/* Delete previously created product attribute for sync */
$model = Mage::getResourceModel('catalog/setup','catalog_setup');
$model->removeAttribute('catalog_product','boxx_product_flag');

/* create product sync table */
$installer = $this;

$installer->startSetup();

$connection = $installer->getConnection();

$productsync_table = 'boxx_productsync';

$installer->run("DROP TABLE IF EXISTS `{$productsync_table}`");

$installer->run("
CREATE TABLE `{$productsync_table}` (
  `product_id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT 0,
  `store_id` smallint(5) unsigned NOT NULL,
  `last_synced_at` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`, `parent_id`, `store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

/* Delete previously created order attribute for sync */
$fieldsSql = 'SHOW COLUMNS FROM sales_flat_order';
$colsorderBlock = $this->getConnection()->fetchCol($fieldsSql);

if (in_array('boxx_order_flag', $colsorderBlock)) {
$installer->run("
    ALTER TABLE {$this->getTable('sales_flat_order')} DROP COLUMN  `boxx_order_flag`;
        ");
}

$fieldsqSql = 'SHOW COLUMNS FROM sales_flat_quote';
$colsquoteBlock = $this->getConnection()->fetchCol($fieldsqSql);
if (in_array('boxx_order_flag', $colsquoteBlock)) {
$installer->run("
    ALTER TABLE {$this->getTable('sales_flat_quote')} DROP COLUMN `boxx_order_flag`;
    ");
}

$ordersync_table = 'boxx_ordersync';
$installer->run("DROP TABLE IF EXISTS `{$ordersync_table}`");

$installer->run("
CREATE TABLE `{$ordersync_table}` (
  `order_id` int(10) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `last_synced_at` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY (`order_id`,  `store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

}catch (Exception $e){
Mage::log("Exception in upgrade script 1.0.1:".$e->getMessage(),null, 'boxx_exception.log');   
}

