<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * Boxx Ai Setup Installer
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author     AIBOXX Solutions Pvt Ltd
 * @copyright Copyright (c) 2016-2018 AIBOXX Solutions Pvt Ltd (https://boxx.ai)
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @user       Boxx Team
*/
try
{
    $boxx_installer = $this;
    $boxx_installer->startSetup();
    $boxxsetup = new Mage_Eav_Model_Entity_Setup('core_setup');

    //Product Start 
    $boxx_attribute  = array(
        'group'=> 'Boxx Ai',
        'type'=>'varchar',  
        'backend_type'=> 'varchar',
        'sort_order'=> 50,
        'label'=>'Boxx Product Synced',
        'global'=>Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible'=> 1,
        'required'=>'0',
        'comparable'=>'0',
        'searchable'=>'0',
        'is_configurable'=>'0',
        'user_defined'=>false,
        'visible_on_front' => 0,
        'visible_in_advanced_search' => 0,
        'is_html_allowed_on_front' => 0,
        'unique'=> false,
        "note"=> "Dont Change This",
        'default' => '1'
    );
    $boxx_installer->addAttribute('catalog_product', 'boxx_product_flag', $boxx_attribute);

    //Adding Drop Down
    $boxxprodEntityTypeId = $boxx_installer->getEntityTypeId('catalog_product');
    $boxx_attribute  = array(
        'label'=>'Magic',
        'global'=>Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible' => 0
    );

    $boxx_installer->addAttribute($boxxprodEntityTypeId, 'relevance_ai',$boxx_attribute);
    $boxx_installer->updateAttribute($boxxprodEntityTypeId, 'relevance_ai', 'used_for_sort_by', 0);

    //Customer Start 
    /*
    $boxxentityTypeId     = $boxxsetup->getEntityTypeId('customer');
    $boxx_attributeSetId   = $boxxsetup->getDefaultAttributeSetId($boxxentityTypeId);
    $boxx_attributeGroupId = $boxxsetup->getDefaultAttributeGroupId($boxxentityTypeId, $boxx_attributeSetId);
    $boxx_installer->addAttribute("customer", "boxx_customer_flag",  array(
        "type"     => "varchar",
        "backend"  => "",
        "label"    => "Boxx Customer Synced",
        "input"    => "text",
        "source"   => "",
        "visible"  => true,
        "required" => false,
        "default" => "1",
        "frontend" => "",
        "unique"     => false,
        "note"       => "Dont Change This"
    ));

    $boxx_attribute   = Mage::getSingleton("eav/config")->getAttribute("customer", "boxx_customer_flag");
    $boxxsetup->addAttributeToGroup(
        $boxxentityTypeId,
        $boxx_attributeSetId,
        $boxx_attributeGroupId,
        'boxx_customer_flag',
        '999'  //sort_order
    );
    $boxxused_in_forms=array();
    $boxxused_in_forms[]="adminhtml_customer";
    $boxx_attribute->setData("used_in_forms", $boxxused_in_forms)
            ->setData("is_used_for_customer_segment", true)
            ->setData("is_system", 0)
            ->setData("is_user_defined", 1)
            ->setData("is_visible", 1)
            ->setData("sort_order", 100)
            ;
    $boxx_attribute->save();
    $boxx_installer->endSetup();
	*/
	$boxx_installer->endSetup();
    //Order Start
    $boxx_installer = $this;
    $boxx_installer = new Mage_Sales_Model_Mysql4_Setup;
    $boxx_attribute  = array(
        'group'=> 'Boxx Ai',
        'type'=>'varchar',  
        'backend_type'=> 'varchar',
        'sort_order'=> 50,
        'label'=>'Boxx Synced',
        'visible'=> 1,
        'required'=>'0',
        'comparable'=>'0',
        'searchable'=>'0',
        'is_configurable'=>'0',
        'user_defined'=>false,
        'visible_on_front' => 0,
        'visible_in_advanced_search' => 0,
        'is_html_allowed_on_front' => 0,
        'unique'=> false,
        "note"=> "Dont Change This",
        'default' => '1'
    );
    $boxx_installer->addAttribute('order', 'boxx_order_flag', $boxx_attribute);
    $boxx_installer->addAttribute('quote', 'boxx_order_flag', $boxx_attribute);
    $boxx_installer->endSetup();
}catch (Exception $e) {
    Mage::log("Exception in installer 1.0.0:".$e->getMessage(),null, 'boxx_exception.log');        
}catch(Error $e){
    Mage::log("Error in installer 1.0.0:".$e->getMessage(),null, 'boxx_exception.log');    
}