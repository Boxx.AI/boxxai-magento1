<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * Boxx Ai Data Settings
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author	   AIBOXX Solutions Pvt Ltd
 * @copyright Copyright (c) 2016-2018 AIBOXX Solutions Pvt Ltd (https://boxx.ai)
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @user 	   Boxx Team
 */
class Boxx_Ai_Helper_Data extends Mage_Core_Helper_Abstract
{


    const LOG_FILE = "boxx_exception.log";

    const ID_SEPARATOR = "-";

    const SANITISE_STRING = "/:|,|;/";

	public function readLogFilesFromEnd($filename, $lines, $revers = false)
	{
		//exception and error handling at usage
		$offset = -1;
		$c = '';
		$read = '';
		$i = 0;
		$fp = @fopen($filename, "r");
		while( $lines && fseek($fp, $offset, SEEK_END) >= 0 ) {
			$c = fgetc($fp);
			if($c == "\n" || $c == "\r"){
				$lines--;
				if( $revers ){
					$read[$i] = strrev($read[$i]);
					$i++;
				}
			}
			if( $revers ) $read[$i] .= $c;
			else $read .= $c;
			$offset--;
		}
		fclose ($fp);
		if( $revers ){
			if($read[$i] == "\n" || $read[$i] == "\r")
				array_pop($read);
			else $read[$i] = strrev($read[$i]);
			return implode('',$read);
		}
		return strrev(rtrim($read,"\n\r"));
	}
	public function readLogFilesFromStart($filename,$lines)
	{
		//exception and error handling at usage
		if(!$filename)
		{
			return;
		}
		$file = new SplFileObject($filename);
		$read="";
		$index=0;
		while($index <= $lines) {
			if (!$file->eof()) {
				 $file->seek($index);
				 $read .= $file->current();
			}
			$index++;
		}
		return $read;
	}

	/**
     * Format bytes into a human readable representation, e.g.
     * 6815744 => 6.5M
     *
     * @param     $bytes
     * @param int $precision
     *
     * @return string
     */
    public function bytesToHumanReadable($bytes, $precision = 2) {
        $suffixes = array("", "k", "M", "G", "T", "P");
        $base = log($bytes) / log(1024);
        return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
    }

    /**
     * Convert human readable formatting of bytes to bytes, e.g.
     * 6.5M => 6815744
     *
     * @param $string
     *
     * @return int
     */
    public function humanReadableToBytes($string) {
        $suffix = strtolower(substr($string, -1));
        $result = substr($string, 0, -1);

        switch ($suffix) {
            case 'g': // G is the max unit as of PHP 5.5.12
                $result *= 1024;
            case 'm':
                $result *= 1024;
            case 'k':
                $result *= 1024;
                break;
            default:
                $result = $string;
        }

        return ceil($result);
    }

    /**
     * Write a log message to the Klevu_Search log file.
     *
     * @param int    $level
     * @param string $message
     */
    public function log($level, $message) {        
        Mage::log($message, $level, static::LOG_FILE);        
    }

        /**
     * Run a sync from cron at the specified time. Checks that a cron is not already
     * scheduled to run in the 15 minute interval before or after the given time first.
     *
     * @param DateTime|string $time The scheduled time as a DateTime object or a string
     *                              that is going to be passed into DateTime. Default is "now".
     *
     * @return $this
     */
    public function schedule($time = "now", $job_code) {
        if (! $time instanceof DateTime) {
            $time = new DateTime($time);
        } else {
            // Don't modify the original parameter
            $time = clone $time;
        }
        $time_str = $time->format("Y-m-d H:i:00");
        $before_str = $time->modify("15 minutes ago")->format("Y-m-d H:i:00");
        $after_str = $time->modify("30 minutes")->format("Y-m-d H:i:00"); // Modifying the same DateTime object, so it's -15 + 30 = +15 minutes

        // Check if Product Sync is not already scheduled to run around (±15 minutes) that time
        $collection = Mage::getResourceModel('cron/schedule_collection')
            ->addFieldToFilter("job_code", $job_code)
            ->addFieldToFilter("status", Mage_Cron_Model_Schedule::STATUS_PENDING)
            ->addFieldToFilter("scheduled_at", array(
                "from" => $before_str,
                "to" => $after_str
            ));

        if ($collection->getSize() == 0) {
            $schedule = Mage::getModel('cron/schedule');
            $schedule
                ->setJobCode($job_code)
                ->setCreatedAt($time_str)
                ->setScheduledAt($time_str)
                ->setStatus(Mage_Cron_Model_Schedule::STATUS_PENDING)
                ->save();
        }

        return $this;
    }


    /**
    * Check if a sync is currently running from cron. A number of running copies to
    * check for can be specified, which is useful if checking if another copy of sync
    * is running from sync itself.
    *
    * Ignores processes that have been running for more than an hour as they are likely
    * to have crashed.
    *
    * @param int $copies
    *
    * @return bool
    */
    public function isRunning($copies = 1, $job_code) {
        $time = new Datetime("1 hour ago");
        $time = $time->format("Y-m-d H:i:00");

        $collection = Mage::getResourceModel('cron/schedule_collection')
            ->addFieldToFilter("job_code", $job_code)
            ->addFieldToFilter("status", Mage_Cron_Model_Schedule::STATUS_RUNNING)
            ->addFieldToFilter("executed_at", array("gteq" => $time));

        return $collection->getSize() >= $copies;
    }

    public function markProductsForUpdate($productid) {
        $connection = Mage::getSingleton('core/resource');

        $write = Mage::getSingleton("core/resource")->getConnection("core_write");

        $query = "update ".$connection->getTableName('ai/productsync')
                ." SET "
                ."last_synced_at = 0 "
                ."where product_id = :product_id";

        $binds = array(
            'product_id' => $productId
        );
        $write->query($query, $binds);
    }

}