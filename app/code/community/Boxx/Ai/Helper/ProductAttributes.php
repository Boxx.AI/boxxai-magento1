<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * Boxx Ai Product Attributes helper
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author	   AIBOXX Solutions Pvt Ltd
 * @copyright Copyright (c) 2016-2018 AIBOXX Solutions Pvt Ltd (https://boxx.ai)
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @user 	   Boxx Team
 */
class Boxx_Ai_Helper_ProductAttributes extends Mage_Core_Helper_Abstract
{
	public function getProductAttributes($productid=null)
	{
		if($productid!=null)
		{			
			$boxxSyncProductId = $productid;
			$boxxAttrSyncProduct = Mage::getModel('catalog/product')->load($boxxSyncProductId);
			$boxxProdAttributes = $boxxAttrSyncProduct->getAttributes();
			$boxxProdAttributeFinder=array();
			$boxxProdAttributeFinderData=array();
			$boxxProdAttributeFinderDataFormat=array();
			
			foreach ($boxxProdAttributes as $boxxProdAttribute) {   
				
				$boxxProdattributeCode = $boxxProdAttribute->getAttributeCode();
				
				if($boxxProdattributeCode=="category_ids" || $boxxProdattributeCode=="relevance_ai" || $boxxProdattributeCode=="boxx_product_flag")
				{
					continue;
				}
				
				$boxxProdvalue = $boxxProdAttribute->getFrontend()->getValue($boxxAttrSyncProduct);
				
				if($boxxProdvalue==null || $boxxProdvalue=="")
				{
					continue;
				}
				
				$boxxProdattributeType = $boxxProdAttribute->getFrontendInput();

				if($boxxProdattributeType=="media_image")
				{
					$boxxMedia_baseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
					$boxxProdvalue=$boxxMedia_baseUrl.'catalog/product'.$boxxProdvalue;
				}
				
				$boxxProdattributeBkType = $boxxProdAttribute->getBackendType();
				
				$boxxProdlabel = $boxxProdAttribute->getFrontendLabel($boxxAttrSyncProduct);    
				
				$boxxProdAttributeFinderData[$boxxProdattributeCode]=$boxxProdvalue;
				if($boxxProdattributeBkType=="datetime" || $boxxProdattributeCode=="created_at" || $boxxProdattributeCode=="updated_at")
				{
					$boxxProdAttributeFinderDataFormat[$boxxProdattributeCode]="date";      
				}			
			} 
			$boxxProdAttributeFinder['data']=$boxxProdAttributeFinderData;
			$boxxProdAttributeFinder['dataformat']=$boxxProdAttributeFinderDataFormat;
			return $boxxProdAttributeFinder;
		}
	}
}