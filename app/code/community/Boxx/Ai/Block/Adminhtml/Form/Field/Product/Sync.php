<?php

/**
 * 
 *
 * @method setStoreId($id)
 * @method string getStoreId()
 */
 
class Boxx_Ai_Block_Adminhtml_Form_Field_Product_Sync extends Mage_Adminhtml_Block_System_Config_Form_Field {

    protected function _prepareLayout() {
        parent::_prepareLayout();

        // Set the default template
        if (!$this->getTemplate()) {
            $this->setTemplate('boxx/form/field/sync/sync.phtml');
        }

        return $this;
    }

    public function render(Varien_Data_Form_Element_Abstract $element) {
        if ($element->getScope() == "stores") {
            $this->setStoreId($element->getScopeId());
        }

        // Remove the scope information so it doesn't get printed out
        $element
            ->unsScope()
            ->unsCanUseWebsiteValue()
            ->unsCanUseDefaultValue();

        return parent::render($element);
    }

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
        // $url_params = array("debug" => "ai",'_store' => Mage::app()->getDefaultStoreView()->getId());
        $label_suffix = ($this->getStoreId()) ? " for This Store" : "";

        $this->addData(array(
            "html_id"         => $element->getHtmlId(),
            "button_label"    => sprintf("Product Sync"),
            "destination_url" => $this->getUrl("adminhtml/boxx_ai/productsync")
        ));

        return $this->_toHtml();
    }
}
