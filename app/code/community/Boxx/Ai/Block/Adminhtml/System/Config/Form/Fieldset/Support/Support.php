<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * Boxx Ai Include Plugin Instructions in Admin Config
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author	   AIBOXX Solutions Pvt Ltd
 * @copyright Copyright (c) 2016-2018 AIBOXX Solutions Pvt Ltd (https://boxx.ai)
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @user 	   Boxx Team
*/

class Boxx_Ai_Block_Adminhtml_System_Config_Form_Fieldset_Support_Support
    extends Mage_Adminhtml_Block_System_Config_Form_Fieldset
{
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
		try{
			$boxxHtml = $this->_getHeaderHtml($element);
			$boxxCurl = curl_init();
			curl_setopt_array($boxxCurl, array(
			CURLOPT_URL => "http://boxxtest.in/static/plugins/instructions.html",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",			
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
			  ),
			));
			$boxxResponse = curl_exec($boxxCurl);
			$boxxErr = curl_error($boxxCurl);
			curl_close($boxxCurl); 
			if ($boxxErr) {
			   $boxxHtml .= "";
			}
			else
			{
				$boxxHtml .= $boxxResponse;
			}		
			$boxxHtml .= $this->_getFooterHtml($element);
			return $boxxHtml;
		}catch (Exception $e) {
			Mage::log("Exception in support block:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in support block:".$e->getMessage(),null, 'boxx_exception.log');	
		}
    }
}
