<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * Boxx Ai Home Block
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author	   AIBOXX Solutions Pvt Ltd
 * @copyright Copyright (c) 2016-2018 AIBOXX Solutions Pvt Ltd (https://boxx.ai)
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @user 	   Boxx Team
*/

//Including Boxx Ai Recommendation SDK 
class Boxx_Ai_Block_Recommendation extends Mage_Catalog_Block_Product_Abstract 
{
	/**
     * Name of request parameter for page number value
     */
    public function __construct() {
		parent::_construct();
		$this->addColumnCountLayoutDepend('empty', 6)
		->addColumnCountLayoutDepend('one_column', 5)
		->addColumnCountLayoutDepend('two_columns_left', 4)
		->addColumnCountLayoutDepend('two_columns_right', 4)
		->addColumnCountLayoutDepend('three_columns', 3);			
    }
	
	
	protected function _getProductCollection()  {
		
		if ($this->getWidgetStatus()=='disable') {
            return '';
        }
		
		$boxxCurrentUrl = $this->getCurrentUrl();		
		$boxxWidget_type = $this->getWidgetType();
		$boxxwidget_instance = $this->getWidgetTrackingCode();
		$boxxpage_url = $boxxCurrentUrl;
		$boxxstoreId = Mage::app()->getStore()->getStoreId();
		$boxxcustomer_id = Mage::getStoreConfig('ai/ai_group/customer_id', $boxxstoreId);	//Client ID					
		$boxxcustomer_key = Mage::getStoreConfig('ai/ai_group/customer_key', $boxxstoreId);	//Access Token		
		$boxxoptimise_type = Mage::getStoreConfig('ai/ai_group/optimise_type', $boxxstoreId); // Optimise Type Widget Count
		$boxxhome_widget_count=$this->getWidgetCount();
		if($this->getWidgetType()=='product')
		{
			$boxxproductRecID = Mage::getSingleton('core/session')->getBoxxCurrentProductRecID();
			$boxxcProduct=Mage::getModel('catalog/product');
			$boxxcProduct->load($boxxproductRecID['product_id']);
			$boxxcategories=$boxxcProduct->getCategoryIds();
		}
		
		$boxxcurl = curl_init();
		curl_setopt_array($boxxcurl, array(
		CURLOPT_URL => "http://app.boxx.ai/client/validate/?client_id=".$boxxcustomer_id."&access_token=".$boxxcustomer_key."&primary_transaction_type=".$boxxoptimise_type."",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",			
		CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
		  ),
		));
		$boxxresponse = curl_exec($boxxcurl);
		$boxxerr = curl_error($boxxcurl);
		curl_close($boxxcurl);			
		if ($boxxerr) {
		  return null;
		}		
		$boxxresponseArray=json_decode($boxxresponse,true);
		if($boxxresponseArray['data']['valid'])
		{						
			$boxxapi_instance = new Boxx\Ai\Recco\Api\RecommendationApi();
			
			$BOXX_CLIENT_ID = $boxxresponseArray['data']['client_id'];
			$BOXX_CHANNEL_ID = $boxxresponseArray['data']['channel_id'];
			$BOXX_ACCESS_TOKEN = $boxxresponseArray['data']['access_token'];
			
			$boxx_query = new \Boxx\Ai\Recco\Model\RecommendationRequestDataQuery();
			
			if(Mage::getSingleton('customer/session')->isLoggedIn()) {
				$boxx_customerData = Mage::getSingleton('customer/session')->getCustomer();
				$boxx_query->setUserid($boxx_customerData->getId()); // for logged in user
			}
			else
			{	
				$boxx_token=Mage::getSingleton('core/session')->getBoxxTokenSession();
				$boxx_query->setBoxxTokenId($boxx_token['boxxtokenid']); // for not logged in user
			}			
			$boxx_query->setNum(intval($boxxhome_widget_count)); // number of recomendations you want. Maximum 50.
			if($this->getWidgetType()=='product')
			{
				$boxx_itemfilter['store_ids'] = $boxxstoreId;

				if(!is_null($boxxcategories[0])){
					$boxx_itemfilter['categories'] = $boxxcategories[0];					
				}


				$boxx_related_products = array($boxxproductRecID['product_id']);
				$boxx_related_action_type = "ProductView";
				$boxx_query->setRelatedProducts($boxx_related_products);
				$boxx_query->setRelatedActionType($boxx_related_action_type);
			}
			else
			{
				$boxx_itemfilter = array(
					"store_ids" => $boxxstoreId
				);
			}
			
			if($this->getWidgetType()=='cart')
			{
				$boxx_cart = Mage::getModel('checkout/cart')->getQuote();				
				$boxx_cartItemsArray=array();
				foreach ($boxx_cart->getAllVisibleItems() as $item) {
					$boxxproductId=$item->getProduct()->getId();
					if($item->getProduct()->getTypeId()=="configurable")
					{
						$boxxsimpleProduct=Mage::getModel('catalog/product')->loadByAttribute('sku', $item->getSku());
						$boxxproductId=$boxxsimpleProduct->getId();
					}			
					array_push($boxx_cartItemsArray,$boxxproductId);
				}
				$boxx_related_products = $boxx_cartItemsArray;
				$boxx_related_action_type = "OrderPlaced";
				$boxx_query->setRelatedProducts($boxx_related_products);
				$boxx_query->setRelatedActionType($boxx_related_action_type);	
			}
			
			$boxx_query->setItemFilters($boxx_itemfilter);
			
			$boxx_request_data = new \Boxx\Ai\Recco\Model\RecommendationRequestData(); 
			$boxx_request_data->setClientId($BOXX_CLIENT_ID);
			$boxx_request_data->setChannelId($BOXX_CHANNEL_ID);
			$boxx_request_data->setAccessToken($BOXX_ACCESS_TOKEN);
			$boxx_request_data->setQuery($boxx_query);
			//Tracking Data
			$boxx_request_data->setWidgetInstance($boxxwidget_instance);
			$boxx_request_data->setWidgetType($boxxWidget_type);
			$boxx_request_data->setPageUrl($boxxpage_url);
			$boxxrecommendedProducts=array();

			try {			
				$boxxresult = $boxxapi_instance->rootPost($boxx_request_data);
				foreach($boxxresult->getResult() as $key => $value){
					array_push($boxxrecommendedProducts,$value->id);
				}								
			} catch (Exception $e) {
				Mage::log("Exception when calling RecommendationApi->rootPost:".$e->getMessage(),null, 'boxx_exception.log');
			}	
			

			if(count($boxxrecommendedProducts) > 0)
			{ 
				$boxxproductsToFetch=array();
				foreach($boxxrecommendedProducts as $id)
				{			
					$boxxproduct = Mage::getModel('catalog/product')->load($id);
					$boxxinStock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($boxxproduct)->getIsInStock();
					if($boxxinStock)
					{
						$boxxproductsToFetch[]=$id;
					}			
				}		
			}
			else
			{
				return null;
			}
			//$collection = Mage::getResourceModel('catalog/product_collection');
			$collection = Mage::getModel('catalog/product')->getCollection();
			$collection->addAttributeToSelect('*');
			//$collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
			//$collection = $this->_addProductAttributesAndPrices($collection)
			$collection->addAttributeToFilter('entity_id', array('in' => $boxxproductsToFetch));
			//$collection->addAttributeToFilter('entity_id', $boxxproductsToFetch);				
			$boxxstoreId    = Mage::app()->getStore()->getId();
			$collection ->setStore($boxxstoreId)
						->addUrlRewrite()
						->addStoreFilter($boxxstoreId)						
						->setPageSize($boxxhome_widget_count)
						->setCurPage(1);
			$collection->getSelect()->order(new Zend_Db_Expr('FIELD(e.entity_id, ' . implode(",", $boxxproductsToFetch).')'));
			return $collection;
		}
		else
		{
			return null;
		}	
    }

	/**
     * Prepare collection with featured products
     *
     * @return Mage_Core_Block_Abstract
     */

    protected function _beforeToHtml(){
		try{
			$this->setWidgetTypeText($this->getWidgetType());
			$this->setHeading($this->getWidgetTitle());
			$this->setWStatus($this->getWidgetStatus());
			$this->setProductCollection($this->_getProductCollection());	
		}catch (Exception $e) {
			Mage::log("Exception in Recommendation block [_beforeToHtml]:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in Recommendation block[_beforeToHtml]:".$e->getMessage(),null, 'boxx_exception.log');	
		}
	}
	
	public function _toHtml(){
		try{
			$boxxtemplateFolder = Mage::getBaseDir('design')."/frontend/".Mage::getSingleton('core/design_package')->getPackageName()."/".Mage::getSingleton('core/design_package')->getTheme('frontend')."/template/";

			$boxxdefaultTemplate="catalog/product/widget/new/content/new_grid.phtml";
			
			$boxxWidgetCustTemplateFile=$this->getWidgetTemplateFile();
			if($boxxWidgetCustTemplateFile!="")
			{
				$boxxTemplate="catalog/product/widget/boxx/".$boxxWidgetCustTemplateFile.".phtml";
			}
			else
			{
				$boxxTemplate="catalog/product/widget/boxx/grid.phtml";
			}			
			if(file_exists($boxxtemplateFolder.$boxxTemplate))
			{
				$this->setTemplate($boxxTemplate);	
			}
			elseif(file_exists($boxxtemplateFolder.$boxxdefaultTemplate))
			{
				$this->setTemplate($boxxdefaultTemplate);	
			}
			else
			{
				Mage::log('template file not found '.date('Y-m-d H:i:s'),null, 'boxxWidget.log');
				return '';
			}
			
			$this->setColumnCount($this->getColumns());			
		}catch (Exception $e) {
			Mage::log("Exception in Recommendation block[_toHtml]:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in Recommendation block[_toHtml]:".$e->getMessage(),null, 'boxx_exception.log');	
		}
		return parent::_toHtml();
    }
	
	/**
     * Render pagination HTML
     *
     * @return string
     */
    public function getPagerHtml()
    {
        return ''; 
    }

}
