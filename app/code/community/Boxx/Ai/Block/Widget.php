<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * Boxx Ai Widget Process
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author	   AIBOXX Solutions Pvt Ltd
 * @copyright Copyright (c) 2016-2018 AIBOXX Solutions Pvt Ltd (https://boxx.ai)
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @user 	   Boxx Team
*/

class Boxx_Ai_Block_Widget extends Mage_Core_Block_Abstract // Mage_Core_Block_Template
 implements Mage_Widget_Block_Interface
{
    public function addData(array $arr){
        $this->_data = array_merge($this->_data, $arr);
    }

    public function setData($key, $value = null){
        $this->_data[$key] = $value;
    }
 
    public function _toHtml(){	
		try{
			
			$boxxstoreId = Mage::app()->getStore()->getStoreId(); //Store id
			$boxxcampaign_id = Mage::getStoreConfig('ai/ai_group/campaign_id', $boxxstoreId); //Campaign ID	 
			//Return If Boxx Credentials Not Correct/Set
			if($boxxcampaign_id=="")
			{
				return;
			}
			
			$boxxPageLayoutRootTemplate=$this->getLayout()->getBlock('root')->getTemplate();
			$boxxCurrentUrl = Mage::helper('core/url')->getCurrentUrl();
			$boxxSiteUrl=Mage::getBaseUrl();
			if (Mage::app()->getStore()->isFrontUrlSecure())
			{
				$boxxSiteUrl=Mage::getBaseUrl($secure = true);
			}
			else
			{
				$boxxSiteUrl=Mage::getBaseUrl();
			}
			$_boxxColumnCount=5;
			switch ($boxxPageLayoutRootTemplate) {
				case 'page/1column.phtml':
					$_boxxColumnCount = 5;
					break;
			 
				case 'page/2columns-left.phtml':
					$_boxxColumnCount = 4;
					break;
			 
				case 'page/2columns-right.phtml':
					$_boxxColumnCount = 4;
					break;
			 
				case 'page/3columns.phtml':
					$_boxxColumnCount = 3;
					break;
			 
				default:
					$_boxxColumnCount = 6;
					break;
			}

			$boxxSetWidgetStatus=$this->getWdStatus();
			$boxxSetWidgetTrackingCode=$this->getWdTrackingCode();
			$boxxSetWidgetType=$this->getWdType();
			$boxxSetWidgetTitle=$this->getWdShowTitle();
			$boxxSetWidgetCount=(int)$this->getWdShowCount();
			$boxxSetWidgetCustTemplate=$this->getWdCustTemplateFile();

			$boxxWidget_settings=array(
				"setWidgetStatus"=>$boxxSetWidgetStatus,
				"setWidgetTrackingCode"=>$boxxSetWidgetTrackingCode,
				"setWidgetType"=>$boxxSetWidgetType,
				"setWidgetTitle"=>$boxxSetWidgetTitle,
				"setWidgetCount"=>$boxxSetWidgetCount,
				"setWidgetCustTemplate"=>$boxxSetWidgetCustTemplate,
				"setCurrentUrl"=>$boxxCurrentUrl,
				"setcolumnCount"=>$_boxxColumnCount
			);
			$boxxPost_widget_settings=base64_encode(json_encode($boxxWidget_settings));

			if ($boxxSetWidgetStatus=='disable') {
				return '';
			}
			$boxxMagic_id=rand(pow(10, 3-1), pow(10, 3)-1);
			$data= '
			<div id="boxx_magic_reccos_'.$boxxMagic_id.'"></div>      
			<script type="text/javascript">	
				var BoxxWidget_Details={url:"'.$boxxSiteUrl.'boxxai/index/getmagic?widget_settings='.$boxxPost_widget_settings.'",divid:"#boxx_magic_reccos_'.$boxxMagic_id.'"};
				BOXX_WIDGETS.push(BoxxWidget_Details);
			</script>
			';
			return $data;
		}catch (Exception $e) {
			Mage::log("Exception in widget block:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in widget block:".$e->getMessage(),null, 'boxx_exception.log');	
		}
	}
}
