<?php

class Boxx_Ai_Adminhtml_Boxx_AiController extends Mage_Adminhtml_Controller_Action {
    protected function productsyncAction(){
    try {
        $store = Mage::app()->getStore($store);
    } catch (Mage_Core_Model_Store_Exception $e) {
        Mage::getSingleton("adminhtml/session")->addError($this->__("Selected store could not be found!"));
        echo $this->_redirectReferer("adminhtml/dashboard");
        exit;
    }
    try{
        Mage::getModel('ai/datasync')->markAllProductsForUpdate($store);
        Mage::getSingleton("adminhtml/session")->addSuccess($this->__("Product Sync scheduled to be run on the next cron"));
    }catch(Exception $e){
        Mage::getSingleton("adminhtml/session")->addError($this->__("Please check the exception log. Try again later"));
        Mage::log($e->getMessage(),null, 'boxx_exception.log');
    }
    echo $this->_redirectReferer();
    exit;
   }

   protected function ordersyncAction()
   {
    try {
    $store = Mage::app()->getStore($store);
    } catch (Mage_Core_Model_Store_Exception $e) {
    Mage::getSingleton("adminhtml/session")->addError("Selected store could not be found!");
    echo $this->_redirectReferer("adminhtml/dashboard");
        exit;
    }

    try{
        Mage::getModel('ai/datasync')->runOrderSyncJob();
        Mage::getSingleton("adminhtml/session")->addSuccess($this->__("Order Sync scheduled to be run on the next cron"));
    }catch(Exception $e){
    Mage::getSingleton("adminhtml/session")->addError($this->__("Please check the exception log. Try again later"));
    Mage::log($e->getMessage(),null, 'boxx_order_exception.log');
    }
    echo $this->_redirectReferer();
    exit;
   }

}
