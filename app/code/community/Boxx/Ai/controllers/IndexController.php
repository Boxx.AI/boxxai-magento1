<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * Boxx Ai Index Controller
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author	   AIBOXX Solutions Pvt Ltd
 * @copyright Copyright (c) 2016-2018 AIBOXX Solutions Pvt Ltd (https://boxx.ai)
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @user 	   Boxx Team
*/

class Boxx_Ai_IndexController extends Mage_Core_Controller_Front_Action
{
   public function indexAction()
   {			
		
   }   
   public function settokenAction()
   {	
		try
		{
			// Set Boxx User Token ID
			$boxx_session = Mage::getSingleton('core/session');
			$boxx_session->setBoxxTokenSession(
				new Varien_Object(array(
					'id' => $this->getRequest()->getParam('boxx_token_id'),
					'boxxtokenid' => $this->getRequest()->getParam('boxx_token_id')
				))
			);		
			return true;
		}catch (Exception $e) {
			Mage::log("Exception in settoken controller:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in settoken controller:".$e->getMessage(),null, 'boxx_exception.log');	
		}
   }
   public function getmagicAction()
   {
   		try{
			$boxx_widget_settings=(($this->getRequest()->getParam('widget_settings'))?$this->getRequest()->getParam('widget_settings'):null);
			if($boxx_widget_settings==null)
			{
				return;
			}
			else
			{
				// Load Widget
				$boxx_get_widget_settings=json_decode(base64_decode($boxx_widget_settings));
				$this->getResponse()->setBody(
					$this->getLayout()->createBlock("ai/recommendation")
					->setData('widget_status',$boxx_get_widget_settings->setWidgetStatus)
					->setData('widget_tracking_code',$boxx_get_widget_settings->setWidgetTrackingCode)
					->setData('widget_type',$boxx_get_widget_settings->setWidgetType)
					->setData('widget_title',$boxx_get_widget_settings->setWidgetTitle)
					->setData('widget_count',$boxx_get_widget_settings->setWidgetCount)
					->setData('widget_template_file',$boxx_get_widget_settings->setWidgetCustTemplate)
					->setData('current_url',$boxx_get_widget_settings->setCurrentUrl)
					->setData('columns',$boxx_get_widget_settings->setcolumnCount)
					->toHtml()
				);
			}
		}catch (Exception $e) {
			Mage::log("Exception in getmagic controller:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in getmagic controller:".$e->getMessage(),null, 'boxx_exception.log');	
		}
   }
   
   public function getlogsAction()
   {	      
	   //@@Request@@
	   // Log Type
	   // Number of line
	   // Start / End
	   
	   if(($this->getRequest()->getParam('type')=="") or ($this->getRequest()->getParam('lines')==""))
	   {
		   return;
	   }
	   $boxxReadLogName="";
	   switch ($this->getRequest()->getParam('type')) {
			case "productsync":
				$boxxReadLogName="boxx_productSync";
				break;
			case "productflag":
				$boxxReadLogName="boxx_productflag";
				break;
			case "customersync":
				$boxxReadLogName="boxx_customerSync";
				break;
			case "customerflag":
				$boxxReadLogName="boxx_customerflag";
				break;
			case "ordersync":
				$boxxReadLogName="boxx_orderSync";
				break;
			case "orderflag":
				$boxxReadLogName="boxx_orderflag";
				break;
			case "widget":
				$boxxReadLogName="boxxWidget";
				break;
			case "exceptions":
				$boxxReadLogName="boxx_exception";
				break;
			default:
				$boxxReadLogName="boxx_exception";
		}
		
		$boxxLogFileToRead=Mage::getBaseDir()."/var/log/".$boxxReadLogName.".log";
		$boxxLogsData;
		try{
			if(file_exists($boxxLogFileToRead))
			{			
				if(!is_readable($boxxLogFileToRead))
				{
					$boxxLogsData="File not readable";
				}
				else
				{		
					if($this->getRequest()->getParam('position')=="start")
					{
						$boxxLogsData=Mage::helper('ai/data')->readLogFilesFromStart($boxxLogFileToRead,intval($this->getRequest()->getParam('lines')));
					}
					else
					{				
						$boxxLogsData=Mage::helper('ai/data')->readLogFilesFromEnd($boxxLogFileToRead,intval($this->getRequest()->getParam('lines')));				
					}
				}			
			}
			else
			{
				$boxxLogsData="File not found";
			}
			$this->getResponse()->setHeader('Content-Type', 'text/plain')->setBody($boxxLogsData);
		}catch (Exception $e) {
			$this->getResponse()->setHeader('Content-Type', 'text/plain')->setBody("Exception:".$e->getMessage());		
		}catch(Error $e){
			$this->getResponse()->setHeader('Content-Type', 'text/plain')->setBody("Error:".$e->getMessage());			
		}
   }
   
   protected function _isAllowed(){
		return true;
   }
}