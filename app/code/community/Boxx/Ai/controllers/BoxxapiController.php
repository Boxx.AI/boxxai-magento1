<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * Boxx Ai Index Controller
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author	   AIBOXX Solutions Pvt Ltd
 * @copyright Copyright (c) 2016-2018 AIBOXX Solutions Pvt Ltd (https://boxx.ai)
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @user 	   Boxx Team
*/

class Boxx_Ai_BoxxapiController extends Mage_Core_Controller_Front_Action
{
   public function indexAction(){}

   public function getProductChildrenAction(){

   		$_conf_product_ids = $this->getRequest()->getRawBody();
   		$_conf_product_ids = json_decode($_conf_product_ids,true);

   		$product_ids = array();
   		foreach ($_conf_product_ids['products'] as $product) {
	   		$_product = Mage::getModel('catalog/product')->load($product);
	   		//print_r($_product->loadParentProductIds()->getData());
			$_parentIdArray = $_product->loadParentProductIds()->getData();
			if(count($_parentIdArray)){
				$product_ids[$product] = $_parentIdArray;
			}else{
				$product_ids[$product] = null;				
			}

   		}

		echo json_encode($product_ids);
   }

   public function getParentIdsAction()
	{
   		$_conf_product_ids = $this->getRequest()->getRawBody();
   		$_conf_product_ids = json_decode($_conf_product_ids,true);

   		$product_ids = array();

   		foreach ($_conf_product_ids['products'] as $product) {
   			$producturl = $imageurl = $parentIds = null;
		    $parentIds = Mage::getResourceSingleton('catalog/product_type_configurable')
		            ->getParentIdsByChild($product);
		    if (count($parentIds)) {
			    $_parentproduct = Mage::getModel('catalog/product')->load($parentIds[0]);
			    $producturl = ($_parentproduct->getProductUrl())?$_parentproduct->getProductUrl():'';
			    $imageurl = ($_parentproduct->getImage()!='no_selection')?$_parentproduct->getImageUrl():'';
		    }
		    if(!$imageurl){
		    	$_product =  Mage::getModel('catalog/product')->load($product);
		    	$imageurl = $_product->getImageUrl();
		    }
		    if(!$producturl){
		    	$_product =  Mage::getModel('catalog/product')->load($product);
		    	$producturl = $_product->getProductUrl();
		    }
		    if (count($parentIds)) {
		        $product_ids[$product] = array('pid' => $parentIds[0],'producturl' => $producturl,'imageurl' => $imageurl);
		    } else {
		        $product_ids[$product] = array('pid' => null,'producturl' => $producturl,'imageurl' => $imageurl);
		    }   			
   		}
   		echo json_encode($product_ids);
	}

}