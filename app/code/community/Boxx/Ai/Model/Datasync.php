<?php 
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * Boxx Ai Datasync Handler Model
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author	   AIBOXX Solutions Pvt Ltd
 * @copyright Copyright (c) 2016-2018 AIBOXX Solutions Pvt Ltd (https://boxx.ai)
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @user 	   Boxx Team
 */

use Boxx\Ai\Upload\Api\UploadDataApi;

class Boxx_Ai_Model_Datasync extends Mage_Core_Model_Abstract
{


	const RECORDS_PER_PAGE = 100;
	const MEMORY_LIMIT = 0.7;
	const PRODUCT_SYNC_CODE = "boxx_product_sync_cron";
	const ORDER_SYNC_CODE = "boxx_order_sync_cron";
	 /************************** ALL CRON JOBS -- STARTS *****************************/
	 public function productFlagUpdate()
	 {
	 	try
	 	{
		    $storeId = Mage::app()->getStore()->getStoreId();
			$campaign_id = Mage::getStoreConfig('ai/ai_group/campaign_id', $storeId);	//Campaign ID	
			$product_sync = Mage::getStoreConfig('ai/ai_group/product_sync', $storeId);	//product_sync

			// If account details are not present skip
			if($campaign_id == "")
			{
				return true;
			}

			// If Products Sync is active return
			if($product_sync == "active")
			{
				return true;
			}

		    $totalProducts = Mage::getModel('ai/product');  
			$totalProducts->addAttributeToFilter(
				array(
					array('attribute'=>'boxx_product_flag','null' => true),
					array('attribute'=>'boxx_product_flag', 'neq' => '1'),
					array('attribute'=> 'boxx_product_flag','eq' => ''),
					array('attribute'=> 'boxx_product_flag','eq' => 'NO FIELD')
				),
			'',
			'left');
			$totalProducts->getSelect()->limit(10000);					

			$allProductIds=array();
			foreach ($totalProducts as $productids) {
				array_push($allProductIds, $productids->getId());
			}

			if(count($allProductIds)==0)
			{
				Mage::log('product sync flag set to active '.date('Y-m-d H:i:s'),null, 'boxx_productflag.log');
				//do nothing change productSync flag to active;
				Mage::getConfig()->saveConfig('ai/ai_group/product_sync', 'active', 'default', 0);	
				Mage::app()->getCacheInstance()->cleanType("config");
				return true;
			}

			Mage::log('updating product flag - started'.date('Y-m-d H:i:s'),null, 'boxx_productflag.log');

			$attributeCode = 'boxx_product_flag';
			$attributeValue = "1";
			$attrData = array( $attributeCode => $attributeValue );
			Mage::getSingleton('catalog/product_action')
		    ->updateAttributes($allProductIds, $attrData, $storeId);
			Mage::log('updating product flag - ended '.date('Y-m-d H:i:s'),null, 'boxx_productflag.log');
			return true;
		}catch (Exception $e) {
			Mage::log("Exception in Datasync - productFlagUpdate:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in Datasync - productFlagUpdate:".$e->getMessage(),null, 'boxx_exception.log');	
		}
	 }


	public function _productSyncJob()
	 {
		try
	 	{ 
			$storeId = Mage::app()->getStore()->getStoreId();
			$customer_id = Mage::getStoreConfig('ai/ai_group/customer_id', $storeId);		//Client ID				
			$customer_key = Mage::getStoreConfig('ai/ai_group/customer_key', $storeId);		//Access Token	
			$optimise_type = Mage::getStoreConfig('ai/ai_group/optimise_type', $storeId);	//Optimise Type	
			$campaign_id = Mage::getStoreConfig('ai/ai_group/campaign_id', $storeId);	//Campaign ID	
			$product_sync = Mage::getStoreConfig('ai/ai_group/product_sync', $storeId);	//product_sync
			if($campaign_id=='')
			{
				// Unset Magic as default sort order to position
				Mage::getConfig()->saveConfig('catalog/frontend/default_sort_by', 'position', 'default', 0);	
				// Remove Magic from available sort by options
				$eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
				$attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'relevance_ai');	
				$sort = $attributeModel['used_for_sort_by'];
				if($sort) {
					$attributeModel->setUsedForSortBy(0);
					$attributeModel->save();
				}
				return true;
			}

			// If Product Sync is paused return
			if($product_sync == "paused")
			{
				return true;
			}		
					
			
			// get 250 products where boxx_product_flag is 0				
			$toBeSyncProducts = Mage::getModel('ai/product');
			$toBeSyncProducts->addAttributeToFilter('boxx_product_flag',array('eq' => '1'));
			$toBeSyncProducts->getSelect()->limit(250);
			
			$batchIds=array();
			foreach ($toBeSyncProducts as $product) {
				array_push($batchIds, $product->getId());
			}

			if(count($batchIds)==0)
			{
				return;
			}
			
			Mage::log('syncing products'.date('Y-m-d H:i:s'),null, 'boxx_productSync.log');
			
			$CLIENT_ID = $customer_id;
			$ACCESS_TOKEN = $customer_key;
			$api_instance = new Boxx\Ai\Upload\Api\UploadDataApi();
			$instanceData=array();
			$temp_data_format=array();
			foreach($toBeSyncProducts as $productSync)
			{
				// handle any error comes in here with continue and flag maintain				
				$cProduct=Mage::getModel('catalog/product');
				$cProduct->load($productSync->getId());		
				//Mage::log("----------".print_r($cProduct->isSaleable(),true).date('Y-m-d H:i:s'),null, 'boxx_productSync.log');
				$ccProduct=Mage::getModel('cataloginventory/stock_item');
				$ccProduct->loadByProduct($cProduct);
				$cProduct->setData('boxx_product_flag', '2')->getResource()->saveAttribute($cProduct, 'boxx_product_flag');
				
				$categories=(($cProduct->getCategoryIds())?$cProduct->getCategoryIds():"");
				$parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($productSync->getId());
				
				$boxxMedia_baseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
				$boxxProdvalue=$boxxMedia_baseUrl.'catalog/product';
								
				$currentProductProperties = array(
					"boxx_parent_id" => (($parentIds)?$parentIds[0]:$productSync->getId()),
					"price" => intval(($cProduct->getPrice())?$cProduct->getPrice():0),
					"special_price" => (($cProduct->getSpecialPrice())?$cProduct->getSpecialPrice():""),
					"product_url" => (($cProduct->getProductUrl())?$cProduct->getProductUrl():""),
					"base_media_path" => $boxxProdvalue,
					"qty" => intval((($ccProduct->getQty())?$ccProduct->getQty():0)),
					"in_stock" => (($ccProduct->getIsInStock())?1:0),
					"categories" => $categories,
					"store_ids" => (($cProduct->getStoreIds())?$cProduct->getStoreIds():""),
					"visibility_int"=>(($cProduct->getVisibility())?$cProduct->getVisibility():"1")
				);
				$ProductStatus=false;
				$getQty=intval((($ccProduct->getQty())?$ccProduct->getQty():0));
				$getVisibility=(($cProduct->getVisibility())?$cProduct->getVisibility():"1");
				if($cProduct->getStatus()==2)
				{
					//status inactive if product status is disabled
					$ProductStatus=false;
				}
				elseif(/*($getQty==0) or */($ccProduct->getIsInStock()==false)) //check if out of stock
				{
					/*if(($cProduct->getTypeId()=="configurable") or ($cProduct->getTypeId()=="grouped") or ($cProduct->getTypeId()=="bundle"))
					{
						$ProductStatus=true;
					}
					else
					{	*/					
						//status inactive
						$ProductStatus=false;
					//}
				}//visibility check for "not visible individually" and "search"
				elseif(($getVisibility=="1") or ($getVisibility=="3"))
				{				
					if(($parentIds)  AND (($cProduct->getTypeId()=="downloadable") or ($cProduct->getTypeId()=="virtual") or ($cProduct->getTypeId()=="simple")))
					{
						$ProductStatus=true;
					}
					else
					{
						$ProductStatus=false;
					}
				}
				else
				{
					//status active
					$ProductStatus=true;
				}
				
				
				// Get All Attributes
				$boxxProdAttrHelper=Mage::helper('ai/ProductAttributes')->getProductAttributes($productSync->getId());
				
				if($boxxProdAttrHelper['data'])
				{
					$currentProductProperties=array_merge($currentProductProperties,$boxxProdAttrHelper['data']);
				}
				if($boxxProdAttrHelper['dataformat'])
				{					
					$temp_data_format=array_merge($temp_data_format,$boxxProdAttrHelper['dataformat']);
				}
				
				$data_instance = new Boxx\Ai\Upload\Model\ApiuploadproductData();
				$data_instance->setProductid($productSync->getId());
				$data_instance->setProperties($currentProductProperties);
				$data_instance->setActive($ProductStatus);
				$data_instance->setStartDate((($cProduct->getCreatedAt())?$cProduct->getCreatedAt():"")); //optional
				//$data_instance->setEndDate("2017-08-06T08:42:49"); // optional
				array_push($instanceData,$data_instance);
			}
			$data = $instanceData;
			$data_format = array(
				"boxx_parent_id" => "string",
				"price" => "number",
				"special_price" => "number",
				"product_url" => "string",
				"base_media_path" => "string",
				"qty" => "number",
				"in_stock" => "number",
				"categories" => "string",
				"store_ids" => "string",
				"visibility_int" => "string"
			);
			if($temp_data_format)
			{
				$data_format=array_merge($data_format,$temp_data_format);
			}			
			$request_data = new Boxx\Ai\Upload\Model\RequestData1();
			$request_data->setClientId($CLIENT_ID);
			$request_data->setAccessToken($ACCESS_TOKEN);
			$request_data->setData($data);
			$request_data->setDataFormat($data_format);
		}catch (Exception $e) {
			Mage::log("Exception in Datasync - productSyncJob:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in Datasync - productSyncJob:".$e->getMessage(),null, 'boxx_exception.log');	
		}
		
		// Upload Products
		try {
			$result = $api_instance->apiUploadProductPut($request_data);	
			Mage::log('synced'.date('Y-m-d H:i:s'),null, 'boxx_productSync.log');
		} catch (Exception $e) {
			// convert the entire batch status to 1
			$attributeCode = 'boxx_product_flag';
			$attributeValue = "1";
			$attrData = array( $attributeCode => $attributeValue );
			Mage::getSingleton('catalog/product_action')
		    ->updateAttributes($batchIds, $attrData, $storeId);
		    Mage::log('sync exception'.date('Y-m-d H:i:s'),null, 'boxx_productSync.log');
		    Mage::log($e->getMessage(),null, 'boxx_productSync.log');
			Mage::log("Exception when calling UploadDataApi->apiUploadProductPut:".$e->getMessage(),null, 'boxx_exception.log');		
		}
		return true;
	 }

	 public function run(){
	 	/*fetching various store*/
	 	Mage::log("Fetching Store Data",null,'boxx_exception.log');
	 	$helper = Mage::helper('ai');

	 	if ($helper->isRunning(2, static::PRODUCT_SYNC_CODE)) {
            Mage::log("Stopping ".static::PRODUCT_SYNC_CODE." because another copy is already running.",null, 'boxx_exception.log');
            return;
        }
        
	 	$stores = Mage::app()->getStores();
	 	foreach ($stores as $store) {	 		
	 		$this->productSyncJob($store);
	 	}
	 }

	 public function productSyncJob($store){

		if ($this->rescheduleIfOutOfMemory()) {
			Mage::log("Memory Limit Exceeded ",null,"boxx_exception.log");
			return;
		}

	 	try{
	 		Mage::log("Starting Sync Proccess",null,'boxx_exception.log');
	 		$connection = Mage::getSingleton('core/resource');
	 		//$storeId = Mage::app()->getWebsite(true)->getDefaultGroup()->getDefaultStoreId();
	 		$storeId = $store->getId();
	 		$sync_query = array(
	 			'delete' => Mage::getModel("ai/productsync")->getCollection()
                    ->getSelect()
                    ->joinLeft(
						array('v' => $connection->getTableName("catalog/category_product_index")),
						"v.product_id = main_table.product_id AND main_table.store_id = $storeId",
						""
					)->joinLeft(
						array('e' => $connection->getTableName("catalog/product")),
						"e.entity_id = main_table.product_id",
						""
					)->joinLeft(
                        array('ss' => $this->getProductStatusAttribute()->getBackendTable()),
                        "ss.entity_id = e.entity_id",
                        "ss.value"
                    )->joinLeft(
                        array('st' => $connection->getTableName("cataloginventory/stock_item")),
                        "st.product_id = e.entity_id",
                        ""
                    )->where("(ss.attribute_id = ".$this->getProductStatusAttribute()->getId().")
                    	AND ((ss.value != ".Mage_Catalog_Model_Product_Status::STATUS_ENABLED.")
                    	OR (st.is_in_stock = 0))")
					->group(array('v.product_id'))
					,
				'add' => Mage::getModel("catalog/product")->getCollection()
	 				->getSelect()
	 				->joinLeft(
	 					array('i' => $connection->getTableName("catalog/category_product_index")),
	 					"e.entity_id = i.product_id AND i.store_id = $storeId",""
	 				)->joinLeft(
	 					array("ps" => $connection->getTableName("ai/productsync")),
	 					"e.entity_id = ps.product_id",""
	 				)->joinLeft(
	 					array('s' => $connection->getTableName("catalog/product_super_link")),
	 					'e.entity_id = s.product_id', "s.parent_id"
	 				)->joinLeft(
                        array('ss' => $this->getProductStatusAttribute()->getBackendTable()),
                        "ss.entity_id = e.entity_id",
                        "ss.value"
                    )->joinLeft(
                         array('st' => $connection->getTableName("cataloginventory/stock_item")),
                         "st.product_id = e.entity_id",
                         ""
                    )->where("(ss.attribute_id = ".$this->getProductStatusAttribute()->getId().") 
                    	AND (ss.value = ".Mage_Catalog_Model_Product_Status::STATUS_ENABLED.")
                    	AND (st.is_in_stock != 0)
                    	AND (ps.product_id  IS NULL)
                    	AND (e.type_id != 'configurable')")
                    ->group(array('e.entity_id'))
                    ,
				'update' => Mage::getModel("catalog/product")->getCollection()
	 				->getSelect()
	 				->joinLeft(
	 					array("ps" => $connection->getTableName("ai/productsync")),
	 					"e.entity_id = ps.product_id",""
	 				)->joinLeft(
	 					array('s' => $connection->getTableName("catalog/product_super_link")),
	 					'e.entity_id = s.product_id', "s.parent_id"
	 				)->joinLeft(
                        array('ss' => $this->getProductStatusAttribute()->getBackendTable()),
                        "ss.entity_id = e.entity_id",
                        "ss.value"
                    )->joinLeft(
                         array('st' => $connection->getTableName("cataloginventory/stock_item")),
                         "st.product_id = e.entity_id",
                         ""
                    )->where("(ss.attribute_id = ".$this->getProductStatusAttribute()->getId().")
                    	AND (ss.value = ".Mage_Catalog_Model_Product_Status::STATUS_ENABLED.")
                    	AND (e.updated_at > ps.last_synced_at)
                    	AND (st.is_in_stock != 0)
                    	AND (e.type_id != 'configurable')")
                    ->group(array('e.entity_id'))
                );

			//echo $sync_query['add']->__tostring(); exit;
			//Mage::log($sync_query['delete']->__tostring(),null,'boxx_exception.log');
			//Mage::log($sync_query['update']->__tostring(),null,'boxx_exception.log');
			//Mage::log($sync_query['add']->__tostring(),null,'boxx_exception.log');

			foreach ($sync_query as $action => $statement) {
				$method = $action . "Products";
				//echo $method;
				$read = Mage::getSingleton('core/resource')->getConnection('core_read');
				$products = $read->fetchAll($statement);

				$total = count($products);

				$pages = ceil($total / static::RECORDS_PER_PAGE);

				//Mage::log("Action ## ".$action,null,"boxx_exception.log");
				//Mage::log("Total Count ## ".$total,null,"boxx_exception.log");
				//Mage::log("Pages ## ".$pages,null,"boxx_exception.log");

				for ($page = 1; $page <= $pages; $page++) {

					if ($this->rescheduleIfOutOfMemory()) {
						Mage::log("Memory Limit Exceeded ",null,"boxx_exception.log");
						return;
					}

					$offset = ($page - 1) * static::RECORDS_PER_PAGE;
					if($action == "delete"){$result = $this->callSyncProductApi(array_slice($products, $offset, static::RECORDS_PER_PAGE), $storeId, false);
						$result = $this->callSyncProductApi(array_slice($products, $offset, static::RECORDS_PER_PAGE), $storeId, false);
					}else{						
						$result = $this->callSyncProductApi(array_slice($products, $offset, static::RECORDS_PER_PAGE), $storeId);
					}

					if ($this->rescheduleIfOutOfMemory()) {
						Mage::log("Memory Limit Exceeded ",null,"boxx_exception.log");
						return;
					}

					$result = $this->$method(array_slice($products, $offset, static::RECORDS_PER_PAGE), $storeId);				
				}

			}

			//$this->callSyncApi();

			Mage::log("Ending Sync Proccess",null,'boxx_exception.log');
	 	}catch(Exception $e){
	 		Mage::log($e->getMessage(),null,'boxx_exception.log');
	 	}
	 }

	/**
     * Return the product status attribute model.
     *
     * @return Mage_Catalog_Model_Resource_Eav_Attribute
     */
    protected function getProductStatusAttribute() {
        if (!$this->hasData("status_attribute")) {
            $this->setData("status_attribute", Mage::getSingleton('eav/config')->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'status'));
        }

        return $this->getData("status_attribute");
    }

    protected function addProducts($products,$storeId){

		if ($this->rescheduleIfOutOfMemory()) {
			Mage::log("Memory Limit Exceeded ",null,"boxx_exception.log");
			return;
		}
		$connection = Mage::getSingleton('core/resource');
    	if(!empty($products)){
    		Mage::log("Starting sync proccess for add new product",null,'boxx_exception.log');
    		foreach($products as $productKey => $productValue){
    			//print_r($productValue);
    			$write = Mage::getSingleton("core/resource")->getConnection("core_write");

    			$query = "replace into ".$connection->getTableName('ai/productsync')
						   . "(product_id, parent_id, store_id, last_synced_at) values"
						   . "(:product_id, :parent_id, :store_id, NOW())";

				$binds = array(
					'product_id' => $productValue['entity_id'],
					'parent_id' => ($productValue['parent_id'] ? $productValue['parent_id'] : $productValue['entity_id']),
					'store_id' => $storeId
				);

				$log_value = Mage::getStoreConfig('ai/ai_product_sync/boxx_product_log', $storeId);
				if ($log_value) {
						$last_synced_at = date('Y-m-d H:i:s');
						Mage::log("Add Product Id : ".$productValue['entity_id']." | Sync Date : ".$last_synced_at, null, 'SyncedProducts.log');
					}
				}

				$write->query($query, $binds);
    			//echo "ADD Product ID".$productValue['entity_id']."Parent ID".$productValue['parent_id']."Store ID".$storeId;
    		}
    	}

    protected function updateProducts($products,$storeId){
		if ($this->rescheduleIfOutOfMemory()) {
			Mage::log("Memory Limit Exceeded ",null,"boxx_exception.log");
			return;
		}
		$connection = Mage::getSingleton('core/resource');
    	if(!empty($products)){
    		Mage::log("Starting sync proccess to update new product",null,'boxx_exception.log');
    		foreach($products as $productKey => $productValue){
    			//print_r($productValue);
    			$write = Mage::getSingleton("core/resource")->getConnection("core_write");

    			$query = "update ".$connection->getTableName('ai/productsync')
    					." SET "
       					."last_synced_at = Now() "
    					."where product_id = :product_id "
    					."AND parent_id = :parent_id "
    					."AND store_id = :store_id";


				$binds = array(
					'product_id' => $productValue['entity_id'],
					'parent_id' => ($productValue['parent_id'] ? $productValue['parent_id'] : $productValue['entity_id']),
					'store_id' => $storeId
				);

				$log_value = Mage::getStoreConfig('ai/ai_product_sync/boxx_product_log', $storeId);
				if ($log_value) {
						$last_synced_at = date('Y-m-d H:i:s');
						Mage::log("Update Product Id : ".$productValue['entity_id']." | Sync Date : ".$last_synced_at, null, 'SyncedProducts.log');
					}
				}

				$write->query($query, $binds);
				//echo "UPDATE Product ID".$productValue['entity_id']."Parent ID".$productValue['parent_id']."Store ID".$storeId;
    		}
    	}

    protected function deleteProducts($products,$storeId){
		if ($this->rescheduleIfOutOfMemory()) {
			Mage::log("Memory Limit Exceeded ",null,"boxx_exception.log");
			return;
		}
		$connection = Mage::getSingleton('core/resource');
    	if(!empty($products)){
    		Mage::log("Starting sync proccess to delete product",null,'boxx_exception.log');
    		foreach($products as $productKey => $productValue){
    			//print_r($productValue);
    			$write = Mage::getSingleton("core/resource")->getConnection("core_write");

    			$query = "delete from ".$connection->getTableName('ai/productsync')
    					." where product_id = :product_id "
    					."AND parent_id = :parent_id "
    					."AND store_id = :store_id";

				$binds = array(
					'product_id' => $productValue['product_id'],
					'parent_id' => ($productValue['parent_id'] ? $productValue['parent_id'] : $productValue['product_id']),
					'store_id' => $storeId
				);

				if ($log_value) {
						$last_synced_at = date('Y-m-d H:i:s');
						Mage::log("Delete Product Id : ".$productValue['entity_id']." | Sync Date : ".$last_synced_at, null, 'SyncedProducts.log');
					}
				}

				$write->query($query, $binds);
				//echo "DELETE Product ID".$productValue['entity_id']."Parent ID".$productValue['parent_id']."Store ID".$storeId;
    		}
    	}

    public function callSyncProductApi($products,$storeId, $status = true){

    	try{
			$CLIENT_ID = Mage::getStoreConfig('ai/ai_group/customer_id', $storeId);		//Client ID				
			$ACCESS_TOKEN = Mage::getStoreConfig('ai/ai_group/customer_key', $storeId);		//Access Token	
			$optimise_type = Mage::getStoreConfig('ai/ai_group/optimise_type', $storeId);	//Optimise Type	
			$campaign_id = Mage::getStoreConfig('ai/ai_group/campaign_id', $storeId);	//Campaign ID	
			$product_sync = Mage::getStoreConfig('ai/ai_group/product_sync', $storeId);	//product_sync
			$sync_product = array();
			//require_once(Mage::getBaseDir("lib")."/Boxx/Ai/Upload/Api/UploadDataApi.php");
			
			$api_instance = new UploadDataApi();

			$instanceData=array();
			$temp_data_format=array();

			foreach ($products as $productIndex => $productValue) {
				$producturl = $image_url = '';
				$cProduct=Mage::getModel('catalog/product')->load($productValue['entity_id']);

				$ccProduct=Mage::getModel('cataloginventory/stock_item');
				$ccProduct->loadByProduct($cProduct);

				$categories=(($cProduct->getCategoryIds())?$cProduct->getCategoryIds():"");
				$parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($productValue['entity_id']);
				$producturl = $cProduct->getProductUrl();
				$imageurl = $cProduct->getImageUrl();
				if($parentIds){
					$cParentProduct=Mage::getModel('catalog/product')->load($parentIds[0]);
					$producturl = $cParentProduct->getProductUrl();
					if($cParentProduct->getImage()!='no_selection'){
						$imageurl = $cParentProduct->getImageUrl();
					}
					$parentCategories=(($cParentProduct->getCategoryIds())?$cParentProduct->getCategoryIds():$categories);
				}

				$boxxMedia_baseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
				$boxxProdvalue=$boxxMedia_baseUrl.'catalog/product';

				$currentProductProperties = array(
					"boxx_parent_id" => (($parentIds)?$parentIds[0]:$cProduct->getId()),
					"price" => intval(($cProduct->getPrice())?$cProduct->getPrice():0),
					"special_price" => (($cProduct->getSpecialPrice())?$cProduct->getSpecialPrice():""),
					"product_url" => $producturl,
					"base_media_path" => $boxxProdvalue,
					"image_url" => $imageurl,
					"qty" => intval((($ccProduct->getQty())?$ccProduct->getQty():0)),
					"in_stock" => (($ccProduct->getIsInStock())?1:0),
					"categories" => (($parentIds)?$parentCategories:$categories),
					"store_ids" => (($cProduct->getStoreIds())?$cProduct->getStoreIds():""),
					"visibility_int"=>(($cProduct->getVisibility())?$cProduct->getVisibility():"1")
				);

				$ProductStatus=false;
				$getQty=intval((($ccProduct->getQty())?$ccProduct->getQty():0));
				$getVisibility=(($cProduct->getVisibility())?$cProduct->getVisibility():"1");

				if($status){
					if(($getVisibility=="1") or ($getVisibility=="3"))
					{				
						if(($parentIds)  AND (($cProduct->getTypeId()=="downloadable") or ($cProduct->getTypeId()=="virtual") or ($cProduct->getTypeId()=="simple")))
						{
						$ProductStatus=true;
						}
						else
						{
						$ProductStatus=false;
						}
					}
					else
					{
						$ProductStatus=true;
					}					
				}else{
					$ProductStatus=false;
				}

				// Get All Attributes
				$boxxProdAttrHelper=Mage::helper('ai/ProductAttributes')->getProductAttributes($productValue['entity_id']);

				if($boxxProdAttrHelper['data'])
				{
					$currentProductProperties=array_merge($currentProductProperties,$boxxProdAttrHelper['data']);
				}
				if($boxxProdAttrHelper['dataformat'])
				{					
					$temp_data_format=array_merge($temp_data_format,$boxxProdAttrHelper['dataformat']);
				}

				$sync_product['product_id'] = $cProduct->getId();		
				$sync_product['parent_id'] = (($parentIds)?$parentIds[0]:$cProduct->getId());		
				$sync_product['store_id'] = $storeId;
				$sync_products[] = $sync_product;		
				$data_instance = new Boxx\Ai\Upload\Model\ApiuploadproductData();
				$data_instance->setProductid($cProduct->getId());
				$data_instance->setProperties($currentProductProperties);
				$data_instance->setActive($ProductStatus);
				$data_instance->setStartDate((($cProduct->getCreatedAt())?$cProduct->getCreatedAt():"")); //optional
				//$data_instance->setEndDate("2017-08-06T08:42:49"); // optional
				array_push($instanceData,$data_instance);

			}
			$data = $instanceData;
			$data_format = array(
				"boxx_parent_id" => "string",
				"price" => "number",
				"special_price" => "number",
				"product_url" => "string",
				"base_media_path" => "string",
				"qty" => "number",
				"in_stock" => "number",
				"categories" => "string",
				"store_ids" => "string",
				"visibility_int" => "string"
			);

			if($temp_data_format)
			{
				$data_format=array_merge($data_format,$temp_data_format);
			}

			$request_data = new Boxx\Ai\Upload\Model\RequestData1();
			$request_data->setClientId($CLIENT_ID);
			$request_data->setAccessToken($ACCESS_TOKEN);
			$request_data->setData($data);
			$request_data->setDataFormat($data_format);

    	}catch(Exception $e){
			Mage::log("Exception in Datasync - productSyncJob:".$e->getMessage(),null, 'boxx_exception.log');
			return false;
		}catch(Error $e){
			Mage::log("Error in Datasync - productSyncJob:".$e->getMessage(),null, 'boxx_exception.log');
			return false;
		}

		try {
			//Mage::log("Here -> ",null, 'boxx_exception.log');
			$result = $api_instance->apiUploadProductPut($request_data);	
			//Mage::log($result,null, 'boxx_exception.log');
			//$this->updateProductsSyncData(true,$sync_products);
			return true;
			//Mage::log('synced'.date('Y-m-d H:i:s'),null, 'boxx_productSync.log');
			//Mage::log('synced'.date('Y-m-d H:i:s'),null, 'boxx_exception.log');
		} catch (Exception $e) {
		    Mage::log($e->getMessage(),null, 'boxx_productSync.log');
			Mage::log("Exception when calling UploadDataApi->apiUploadProductPut:".$e->getMessage(),null, 'boxx_exception.log');
			return false;		
		}

		return true;
    }

    public function updateProductsSyncData($flag,$product_ids){
    	$connection = Mage::getSingleton('core/resource');
    	if($flag){
    		if(is_array($product_ids)){
    			//Mage::log($product_ids,null, 'boxx_exception.log');

    			foreach ($product_ids as $product_id) {
    				//Mage::log($product_id,null, 'boxx_exception.log');

    				$write = Mage::getSingleton("core/resource")->getConnection("core_write");

	    			$query = "update ".$connection->getTableName('ai/productsync')
	    					." SET "
	       					."last_synced_at = NOW() "
	    					."where product_id = :product_id "
	    					."AND parent_id = :parent_id "
	    					."AND store_id = :store_id";


					$binds = array(
						'product_id' => $product_id['product_id'],
						'parent_id' => ($product_id['parent_id'] ? $product_id['parent_id'] : '0'),
						'store_id' => $product_id['store_id']
					);

					$write->query($query, $binds);
    			}

    		}
    	}else{

    	}
    }

	public function _orderSyncJob(){
		try
		{
			$storeId = Mage::app()->getStore()->getStoreId();
			$customer_id = Mage::getStoreConfig('ai/ai_group/customer_id', $storeId);		//Client ID				
			$customer_key = Mage::getStoreConfig('ai/ai_group/customer_key', $storeId);		//Access Token	
			$optimise_type = Mage::getStoreConfig('ai/ai_group/optimise_type', $storeId);	//Optimise Type	
			$campaign_id = Mage::getStoreConfig('ai/ai_group/campaign_id', $storeId);	//Campaign ID	
			$order_sync = Mage::getStoreConfig('ai/ai_group/order_sync', $storeId);	//order_sync
			if($campaign_id=='')
			{			
				return true;
			}

			// If Order Sync is paused return
			if($order_sync == "paused")
			{
				return true;
			}		

			
						
			$toBeSyncOrder = Mage::getModel('sales/order')->getCollection();
			$toBeSyncOrder->addAttributeToFilter('boxx_order_flag',array('eq' => '1'));
			$toBeSyncOrder->getSelect()->limit(100);
			
			$batchIds=array();
			foreach($toBeSyncOrder as $order)
			{
				array_push($batchIds, $order->getId());
			}

			if(count($batchIds)==0)
			{
				return;
			}

			Mage::log('syncing orders'.date('Y-m-d H:i:s'),null, 'boxx_orderSync.log');	

			$CLIENT_ID = $customer_id;
			$ACCESS_TOKEN = $customer_key;

			$api_instance = new Boxx\Ai\Upload\Api\UploadDataApi();
			$instanceData=array();
			

			foreach($toBeSyncOrder as $order)
			{
				//Ignore order if customer is not present but change the flag
				
				$order->setData('boxx_order_flag', '2')->getResource()->saveAttribute($order, 'boxx_order_flag');	
				if($order->getCustomerId()!="")
				{
					foreach( $order->getAllVisibleItems() as $item ) {

						$currentTransactionProperties = array(
							"id" => $order->getId()
						);
						
						$data_instance = new Boxx\Ai\Upload\Model\ApiuploadtransactionData();
						
						$data_instance->setTransactionid($order->getId()."_".$item->getProductId());
						$data_instance->setCustomerid($order->getCustomerId());
						$data_instance->setProductid($item->getProductId());
						$data_instance->setType("OrderPlaced");
						$data_instance->setTransactiondate($order->getCreatedAt());
						$data_instance->setProperties($currentTransactionProperties);	
						array_push($instanceData,$data_instance);				
					}
				}
			}
			
			
			$data = $instanceData;
			$data_format = array(
				"id" => "number"
			);			
					
			$request_data = new Boxx\Ai\Upload\Model\RequestData2();
			$request_data->setClientId($CLIENT_ID);
			$request_data->setAccessToken($ACCESS_TOKEN);
			$request_data->setData($data);
			$request_data->setDataFormat($data_format);
		
		}catch (Exception $e) {
			Mage::log("Exception in Datasync - orderSyncJob:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in Datasync - orderSyncJob:".$e->getMessage(),null, 'boxx_exception.log');	
		}
		
		// Upload Order Data
		try {
			if($data)
			{					
				$result = $api_instance->apiUploadTransactionPut($request_data);
			}
			Mage::log('synced'.date('Y-m-d H:i:s'),null, 'boxx_orderSync.log');
		} catch (Exception $e) {				
			// Get Table Details to prepare Query
			$coreResource = Mage::getSingleton('core/resource');
			// Get DB Write Connection
			$writeConnection = $coreResource->getConnection('core_write');
			// Get DB Table name for orders
			$orderTableName = $coreResource->getTableName('sales/order');
			// Prepare Query to update 100 records at once
			$query = "UPDATE {$orderTableName} SET boxx_order_flag = '1' WHERE entity_id IN (" . implode(',', array_map('intval', $batchIds)) . ")";
			// Execute Query
		 	$writeConnection->query($query);
			Mage::log('sync exception'.date('Y-m-d H:i:s'),null, 'boxx_orderSync.log');
		    Mage::log($e->getMessage(),null, 'boxx_orderSync.log');
			Mage::log("Exception when calling UploadDataApi->apiUploadTransactionPut:".$e->getMessage(),null, 'boxx_exception.log');		
		}
		
		return true;
	}

	public function runOrderSyncJob(){
		Mage::log("Fetching Store Data",null,'boxx_order_exception.log');
		$helper = Mage::helper('ai');

	 	if ($helper->isRunning(2, static::ORDER_SYNC_CODE)) {
            Mage::log("Stopping ".static::ORDER_SYNC_CODE." because another copy is already running.",null, 'boxx_order_exception.log');
            return;
        }

        $stores = Mage::app()->getStores();
	 	foreach ($stores as $store) {
	 		$this->orderSyncJob($store);
	 	}
	}

	protected function orderSyncJob($store){
		Mage::log("Fetching Store Data",null,'boxx_order_exception.log');
	 	//$storeId = Mage::app()->getWebsite(true)->getDefaultGroup()->getDefaultStoreId();
	 	$storeId = $store->getId();
		
	 	try{
	 		$connection = Mage::getSingleton('core/resource');
			$storeId = $store->getId();
	 		$sync_query = array(
				'add' => Mage::getModel("sales/order")->getCollection()->getSelect()
				->joinLeft(array("os" => $connection->getTableName("ai/ordersync")),
 					"main_table.increment_id = os.order_id",""
 				)->where("(os.order_id IS NULL)")
                ->group(array('main_table.increment_id'))
                ->limit(200)
            );

	 		foreach ($sync_query as $action => $statement) {
	 			$method = $action . "Orders";

				$read = Mage::getSingleton('core/resource')->getConnection('core_read');
				$orders = $read->fetchAll($statement);

				$total = count($orders);

				$pages = ceil($total / static::RECORDS_PER_PAGE);

				for ($page = 1; $page <= $pages; $page++) {

					$result = $this->callSyncOrderApi(array_slice($orders, $offset, static::RECORDS_PER_PAGE), $storeId);
					if($result){
						$this->$method(array_slice($orders, $offset, static::RECORDS_PER_PAGE), $storeId);
					}
				}
	 		}

	 		$log_value = Mage::getStoreConfig('ai/ai_order_sync/boxx_order_log', $storeId);
		    if ($log_value) {
	 		$readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
			$read_query = "Select order_id, last_synced_at from ".$connection->getTableName('ai/ordersync');
			$results = $readConnection->fetchAll($read_query);
			foreach ($results as $result) {
				$order_id = $result['order_id'];
				$last_synced_at = $result['last_synced_at'];
            Mage::log("Order Id : ".$order_id." | Sync Date : ".$last_synced_at, null, 'SyncedOrders.log');
			}
			}

	 	}catch(Exception $e){
	 		Mage::log($e->getMessage(),null,'boxx_order_exception.log');
	 	}

	}

    protected function addOrders($orders,$storeId){

		if ($this->rescheduleIfOutOfMemory()) {
			Mage::log("Memory Limit Exceeded ",null,"boxx_order_exception.log");
			return;
		}
		$connection = Mage::getSingleton('core/resource');
		$write = Mage::getSingleton("core/resource")->getConnection("core_write");
    	if(!empty($orders)){
    		Mage::log("Starting sync proccess for add new orders",null,'boxx_order_exception.log');
    		foreach($orders as $ordersKey => $ordersValue){
    			//print_r($ordersValue);

    			$query = "replace into ".$connection->getTableName('ai/ordersync')
						   . "(order_id, store_id, last_synced_at) values"
						   . "(:increment_id, :store_id, NOW())";

				$binds = array(
					'increment_id' => $ordersValue['increment_id'],
					'store_id' => $storeId
				);
				$write->query($query, $binds);

    			//echo "ADD Product ID".$productValue['entity_id']."Parent ID".$productValue['parent_id']."Store ID".$storeId;
    		}
    	}
    	//exit;
    }

    public function callSyncOrderApi($orders,$storeId){
		try
		{
			$storeId = Mage::app()->getStore()->getStoreId();
			$customer_id = Mage::getStoreConfig('ai/ai_group/customer_id', $storeId);		//Client ID				
			$customer_key = Mage::getStoreConfig('ai/ai_group/customer_key', $storeId);		//Access Token	
			$optimise_type = Mage::getStoreConfig('ai/ai_group/optimise_type', $storeId);	//Optimise Type	
			$campaign_id = Mage::getStoreConfig('ai/ai_group/campaign_id', $storeId);	//Campaign ID	
			$order_sync = Mage::getStoreConfig('ai/ai_group/order_sync', $storeId);	//order_sync

			Mage::log('syncing orders'.date('Y-m-d H:i:s'),null, 'boxx_orderSync.log');	

			$CLIENT_ID = $customer_id;
			$ACCESS_TOKEN = $customer_key;

			$api_instance = new UploadDataApi();
			$instanceData=array();
			

			foreach($orders as $SyncOrder)
			{
				//Ignore order if customer is not present but change the flag
				$order = Mage::getModel('sales/order')->load($SyncOrder['entity_id']);
				if($order->getCustomerId()!="")
				{
					foreach( $order->getAllVisibleItems() as $item ) {

						$currentTransactionProperties = array(
							"id" => $order->getId()
						);
						
						$data_instance = new Boxx\Ai\Upload\Model\ApiuploadtransactionData();
						
						$data_instance->setTransactionid($order->getId()."_".$item->getProductId());
						$data_instance->setCustomerid($order->getCustomerId());
						$data_instance->setProductid($item->getProductId());
						$data_instance->setType("OrderPlaced");
						$data_instance->setTransactiondate($order->getCreatedAt());
						$data_instance->setProperties($currentTransactionProperties);	
						array_push($instanceData,$data_instance);				
					}
				}
			}
			
			
			$data = $instanceData;
			$data_format = array(
				"id" => "number"
			);
					
			$request_data = new Boxx\Ai\Upload\Model\RequestData2();
			$request_data->setClientId($CLIENT_ID);
			$request_data->setAccessToken($ACCESS_TOKEN);
			$request_data->setData($data);
			$request_data->setDataFormat($data_format);
			//print_r($request_data);
			//exit;
		
		}catch (Exception $e) {
			Mage::log("Exception in Datasync - orderSyncJob:".$e->getMessage(),null, 'boxx_order_exception.log');		
		}catch(Error $e){
			Mage::log("Error in Datasync - orderSyncJob:".$e->getMessage(),null, 'boxx_order_exception.log');	
		}
		
		// Upload Order Data
		try {
			if($data)
			{					
				$result = $api_instance->apiUploadTransactionPut($request_data);
			}
			Mage::log('synced'.date('Y-m-d H:i:s'),null, 'boxx_orderSync.log');
			return true;
		} catch (Exception $e) {
		    Mage::log($e->getMessage(),null, 'boxx_exception.log');
			Mage::log("Exception when calling UploadDataApi->apiUploadTransactionPut:".$e->getMessage(),null, 'boxx_order_exception.log');		
			return false;
		}
		
	}	
	 
	public function updateProductFlags(){
		try
	 	{
			// Get Table Details to prepare Query
			$coreResource = Mage::getSingleton('core/resource');
			// Get DB Write Connection
			$writeConnection = $coreResource->getConnection('core_write');
			// Get DB Read Connection
			$readConnection = $coreResource->getConnection('core_read');
			// Get DB Table name for products
			$eavAttributeTableName = $coreResource->getTableName('eav/attribute');
			$productEntityTableName = $coreResource->getTableName('catalog_product_entity_varchar');
			// Get Product Attributes Details
			$queryEav="select {$eavAttributeTableName}.attribute_id,{$eavAttributeTableName}.entity_type_id from {$eavAttributeTableName} where {$eavAttributeTableName}.attribute_code='boxx_product_flag'";
			$eavAttributes = $readConnection->fetchAll($queryEav);			
			// Update query for all product flags
			$updateProductFlagQuery="update {$productEntityTableName} set value='1' where attribute_id='".$eavAttributes[0]['attribute_id']."' and entity_type_id='".$eavAttributes[0]['entity_type_id']."'";						
			$writeConnection->query($updateProductFlagQuery);			
			Mage::log('all products flags to 1 - from resync cron '.date('Y-m-d H:i:s'),null, 'boxx_productflag.log');
		}catch (Exception $e) {
			Mage::log("Exception in Datasync - productFlagUpdate:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in Datasync - productFlagUpdate:".$e->getMessage(),null, 'boxx_exception.log');	
		}
	}

	/**
     * Check if the current memory usage is below the limit.
     *
     * @return bool
     */
    protected function isBelowMemoryLimit() {
         $helper = Mage::helper('ai');
         $php_memory_limit = ini_get('memory_limit');
         $usage = memory_get_usage(true);

        if($php_memory_limit < 0){
            $this->log(Zend_Log::DEBUG, sprintf(
            "Memory usage: %s of %s.",
            $helper->bytesToHumanReadable($usage),
            $php_memory_limit));
            return true;
        }
        
        $limit = $helper->humanReadableToBytes($php_memory_limit);

        $this->log(Zend_Log::DEBUG, sprintf(
            "Memory usage: %s of %s.",
            $helper->bytesToHumanReadable($usage),
            $helper->bytesToHumanReadable($limit)
        ));

        if ($usage / $limit > static::MEMORY_LIMIT) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Check if the memory limit has been reached and reschedule to run
     * again immediately if so.
     *
     * @return bool true if a new process was scheduled, false otherwise.
     */
    protected function rescheduleIfOutOfMemory() {
    	$helper = Mage::helper('ai');
        if (!$this->isBelowMemoryLimit()) {
			Mage::getSingleton('core/session')->setMemoryMessage("There may be few products left which will be synced during the next sync process.");
            $this->log(Zend_Log::INFO, "Memory limit reached. Stopped and rescheduled.");			
			$helper->schedule("now","boxx_product_sync_cron");			
            return true;
        }

        return false;
    }

	/**
     * Write a message to the log file.
     *
     * @param int    $level
     * @param string $message
     *
     * @return $this
     */
    protected function log($level, $message) {
        Mage::helper('ai')->log($level, sprintf("[%s] %s", static::PRODUCT_SYNC_CODE, $message));

        return $this;
    }

    public function markAllProductsForUpdate($store) {
        $stores = Mage::app()->getStores();
        $connection = Mage::getSingleton('core/resource');
	 	foreach ($stores as $store) {	 		
	 		$write = Mage::getSingleton("core/resource")->getConnection("core_write");

			$query = "update ".$connection->getTableName('ai/productsync')
					." SET "
					."last_synced_at = 0 "
					."where store_id = :store_id";

			$binds = array(
				'store_id' => $store->getStoreId()
			);
			$write->query($query, $binds);
			}
    }

	  
}
