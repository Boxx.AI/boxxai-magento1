<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * Boxx Ai Model System Config Source View - To handle Recommendation Settings Source Model
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author	   AIBOXX Solutions Pvt Ltd
 * @copyright Copyright (c) 2016-2018 AIBOXX Solutions Pvt Ltd (https://boxx.ai)
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @user 	   Boxx Team
 */

class Boxx_Ai_Model_System_Config_Source_Resyncsettings
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => '0 0 31 2 *', 'label' => Mage::helper('adminhtml')->__('Never')),
            array('value' => '*/2 * * * *', 'label' => Mage::helper('adminhtml')->__('every 2 mins')),
            array('value' => '0 * * * *', 'label' => Mage::helper('adminhtml')->__('Hourly')),
            array('value' => '0 */3 * * *', 'label' => Mage::helper('adminhtml')->__('Every 3 hours')),
            array('value' => '0 */6 * * *', 'label' => Mage::helper('adminhtml')->__('Every 6 hours')),
            array('value' => '0 */12 * * *', 'label' => Mage::helper('adminhtml')->__('Every 12 hours')),
            array('value' => '0 22 * * *', 'label' => Mage::helper('adminhtml')->__('Daily')),
            array('value' => '0 22 * * 2', 'label' => Mage::helper('adminhtml')->__('Weekly')),
            array('value' => '0 0 1 * *', 'label' => Mage::helper('adminhtml')->__('Monthly')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            '0 0 31 12 *' => Mage::helper('adminhtml')->__('Never'),
            '0 22 * * *' => Mage::helper('adminhtml')->__('Daily'),
            '0 22 * * 2' => Mage::helper('adminhtml')->__('Weekly'),
            '0 0 1 * *' => Mage::helper('adminhtml')->__('Monthly'),
        );
    }
}
