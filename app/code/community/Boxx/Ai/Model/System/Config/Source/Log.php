<?php

class Boxx_Ai_Model_System_Config_Source_Log
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'Select',  'label'=>Mage::helper('ai')->__('Select')),
            array('value' => 1,  'label'=>Mage::helper('ai')->__('Yes')),
            array('value' => 0,  'label'=>Mage::helper('ai')->__('No'))
        );
    }

    public function toArray()
    {
        return array(
            'Select'  => Mage::helper('ai')->__('Select'),
            1  => Mage::helper('ai')->__('Yes'),
            0  => Mage::helper('ai')->__('No')
        );
    }
}