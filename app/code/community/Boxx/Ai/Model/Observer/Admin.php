<?php 
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * Boxx Ai Admin Observer Handler Model
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author	   AIBOXX Solutions Pvt Ltd
 * @copyright Copyright (c) 2016-2018 AIBOXX Solutions Pvt Ltd (https://boxx.ai)
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @user 	   Boxx Team
 */

class Boxx_Ai_Model_Observer_Admin extends Varien_Event_Observer
{
	
	 /************************** ALL BACKEND INTERACTION -- STARTS *****************************/

	 // Track Product Delete Single and Mass
	 public function productDelete($observer){
	 	try
	 	{	
			$storeId = Mage::app()->getStore()->getStoreId();
			 $campaign_id = Mage::getStoreConfig('ai/ai_group/campaign_id', $storeId);	//Campaign ID	 
			 if($campaign_id=="")
			 {
				return true;
			 }
			$product = $observer->getProduct();
			
			$storeId = Mage::app()->getStore()->getStoreId();
			$customer_id = Mage::getStoreConfig('ai/ai_group/customer_id', $storeId);	//Client ID					
			$customer_key = Mage::getStoreConfig('ai/ai_group/customer_key', $storeId);	//Access Token		
					 
			$CLIENT_ID = $customer_id;
			$ACCESS_TOKEN = $customer_key;

			$api_instance = new Boxx\Ai\Upload\Api\DeleteSpecificApi();

			$data_type = "product"; 

			$data = array($product->getId()); // id of product to delete

			$request_data = new Boxx\Ai\Upload\Model\RequestData3();
			$request_data->setClientId($CLIENT_ID);
			$request_data->setAccessToken($ACCESS_TOKEN);
			$request_data->setData($data);
			$request_data->setDataType($data_type);
		}catch (Exception $e) {
			Mage::log("Exception in admin observer - productDelete:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in admin observer - productDelete:".$e->getMessage(),null, 'boxx_exception.log');	
		}
		
		// Delete Products Api
		try {
			$result = $api_instance->apiDataDeleteDelete($request_data);
		} catch (Exception $e) {
			Mage::log("Exception when calling DeleteSpecific->apiDataDeleteDelete:".$e->getMessage(),null, 'boxx_exception.log');					
		} catch(Error $e){
			Mage::log("Error when calling DeleteSpecific->apiDataDeleteDelete:".$e->getMessage(),null, 'boxx_exception.log');
		}
	 }
	 
	 // Track Product Add/Edit
     public function productAddEditClick($observer){	
     	try{
     		
	     	// Update Sync Flag to 1
			$product = $observer->getProduct();
			// $product->setData('boxx_product_flag', '1')->getResource()
			// ->saveAttribute($product, 'boxx_product_flag');
		}catch (Exception $e) {
			Mage::log("Exception in admin observer - productAddEditClick:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in admin observer - productAddEditClick:".$e->getMessage(),null, 'boxx_exception.log');	
		}
     }
	 


	 // Update Customer Data
	 public function updateCustomerData($observer)
	 {
	 	try
	 	{
		 	//Update Sync Flag to 1
			$customer = $observer->getCustomer();
			$customer->setData('boxx_customer_flag', '1')->getResource()->saveAttribute($customer, 'boxx_customer_flag');	
		}catch (Exception $e) {
			Mage::log("Exception in admin observer - updateCustomerData:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in admin observer - updateCustomerData:".$e->getMessage(),null, 'boxx_exception.log');	
		}
	 }
	 
	 // Delete Customer Data
	 public function deleteCustomerData($observer)
	 {
	 	try
	 	{
			 $storeId = Mage::app()->getStore()->getStoreId();
			 $campaign_id = Mage::getStoreConfig('ai/ai_group/campaign_id', $storeId);	//Campaign ID	 
			 if($campaign_id=="")
			 {
				return true;
			 }
			$customer = $observer->getCustomer();
			
			$storeId = Mage::app()->getStore()->getStoreId();
			$customer_id = Mage::getStoreConfig('ai/ai_group/customer_id', $storeId);	//Client ID					
			$customer_key = Mage::getStoreConfig('ai/ai_group/customer_key', $storeId);	//Access Token		
					 
			$CLIENT_ID = $customer_id;
			$ACCESS_TOKEN = $customer_key;

			$api_instance = new Boxx\Ai\Upload\Api\DeleteSpecificApi();

			$data_type = "customer"; 

			$data = array($customer->getId()); // id of product to delete

			$request_data = new Boxx\Ai\Upload\Model\RequestData3();
			$request_data->setClientId($CLIENT_ID);
			$request_data->setAccessToken($ACCESS_TOKEN);
			$request_data->setData($data);
			$request_data->setDataType($data_type);
		}catch (Exception $e) {
			Mage::log("Exception in admin observer - deleteCustomerData:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in admin observer - deleteCustomerData:".$e->getMessage(),null, 'boxx_exception.log');	
		}
		
		// Delete customer api
		try {
			$result = $api_instance->apiDataDeleteDelete($request_data);
		} catch (Exception $e) {
			Mage::log("Exception when calling DeleteSpecific->apiDataDeleteDelete:".$e->getMessage(),null, 'boxx_exception.log');				
		} catch(Error $e){
			Mage::log("Error when calling DeleteSpecific->apiDataDeleteDelete:".$e->getMessage(),null, 'boxx_exception.log');
		}
	 }
	 
	 public function productAttributeUpdate($observer)
	 {
	  	try
	  	{
		  	 $attributesData=array('blank');
		  	 $productIds = $observer->getEvent()->getProductIds();
			 if($observer->getEvent()->getAttributesData())
			 {
			 	array_push($attributesData, $observer->getEvent()->getAttributesData());
			 }
			 if(array_key_exists("boxx_product_flag", $attributesData))
			 {
		 		//do nothing
			 }
			 else
			 {
			 	foreach($productIds as $product)
			 	{
			 		$productData = Mage::getModel('catalog/product')->load($product);
			 		$productData->setData('boxx_product_flag', '1')->getResource()->saveAttribute($productData, 'boxx_product_flag');			 	
			 	}
			 }
		}catch (Exception $e) {
			Mage::log("Exception in admin observer - productAttributeUpdate:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in admin observer - productAttributeUpdate:".$e->getMessage(),null, 'boxx_exception.log');	
		} 
	  	
	  }

	 /************************** ALL BACKEND INTERACTION -- ENDS *****************************/
	 
	 // Track Configuration Settings of plugin save
     
	 public function adminConfigSave()
     {	 
		try
		{
			$storeId = Mage::app()->getStore()->getStoreId();
			$customer_id = Mage::getStoreConfig('ai/ai_group/customer_id', $storeId);	//Client ID					
			$customer_key = Mage::getStoreConfig('ai/ai_group/customer_key', $storeId);	//Access Token		
			$optimise_type = Mage::getStoreConfig('ai/ai_group/optimise_type', $storeId); // Optimise Type
			$sort_by_boxx_name = Mage::getStoreConfig('ai/ai_group_rec/sort_by_boxx_name', $storeId);	//Campaign ID
			if(Mage::getStoreConfig('ai/ai_group_rec/plp_show', $storeId)=="disable")
			{
				// Unset Magic as default sort order to position
				Mage::getConfig()->saveConfig('catalog/frontend/default_sort_by', 'position', 'default', 0);	
				// Remove Magic from available sort by options
				$eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
				$attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'relevance_ai');	
				$sort = $attributeModel['used_for_sort_by'];
				if($sort) {
					$attributeModel->setUsedForSortBy(0);
					$attributeModel->save();
				}
			}
			if((Mage::getStoreConfig('ai/ai_group_rec/plp_show', $storeId)=="enable"))
			{
				// Add Magic from available sort by options
				$eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
				$attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'relevance_ai');
				$attributeModel->setUsedForSortBy(1);
				$attributeModel->setFrontendLabel($sort_by_boxx_name);
				$attributeModel->save();
				// Set Magic as default sort order to position
				Mage::getConfig()->saveConfig('catalog/frontend/default_sort_by', 'relevance_ai', 'default', 0);
			}
			$existingBoxxCredentials=Mage::getModel('core/session')->getBoxxCredentials();
			if($existingBoxxCredentials['customer_id']!=$customer_id || $existingBoxxCredentials['customer_key']!=$customer_key)
			{
				// Credentials changed
				Mage::getConfig()->saveConfig('ai/ai_group/product_sync', 'paused', 'default', 0);
				Mage::getConfig()->saveConfig('ai/ai_group/customer_sync', 'paused', 'default', 0);
				Mage::getConfig()->saveConfig('ai/ai_group/order_sync', 'paused', 'default', 0);			
				Mage::getConfig()->saveConfig('ai/ai_group_rec/plp_show', 'disable', 'default', 0);
			}
			
			$curl = curl_init();
			curl_setopt_array($curl, array(
			CURLOPT_URL => "http://app.boxx.ai/client/validate/?client_id=".$customer_id."&access_token=".$customer_key."&primary_transaction_type=".$optimise_type."",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",			
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
			  ),
			));
			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);			
			if ($err) {
			  return true;
			}
			$responseArray=json_decode($response,true);		
			if($responseArray['data']['valid']!=true)
			{
				$ai_plugin_url=Mage::helper("adminhtml")->getUrl("adminhtml/system_config/edit/section/ai/");
				Mage::getSingleton('core/session')->addError('Invalid Boxx Ai Configuration');
				$notifications = Mage::getSingleton('ai/notification');
				$notifications->addMessage("<strong style='color:red'>Important: </strong> <b>Invalid Boxx Ai Configuration</b></br>Please fix <a href='".$ai_plugin_url."'>here</a>");
				Mage::getConfig()->saveConfig('ai/ai_group/campaign_id', '', 'default', 0);			
				Mage::getConfig()->saveConfig('ai/ai_group_rec/plp_show', 'disable', 'default', 0);
			}
			else{
				Mage::getConfig()->saveConfig('ai/ai_group/campaign_id', $responseArray['data']['channel_id'], 'default', 0);
			}		
			
			// $product_sync = Mage::getStoreConfig('ai/ai_group/product_sync', $storeId);	//product_sync
			// if($product_sync!='active')
			// {			
			// 	Mage::getConfig()->saveConfig('ai/ai_group_rec/plp_show', 'disable', 'default', 0);
			// 	Mage::getSingleton('core/session')->addWarning('"Boxx Integration Widget" will be enabled post complete data synchronisation');
			// }
			Mage::app()->getCacheInstance()->cleanType("config");
		}catch (Exception $e) {
			Mage::log("Exception in admin observer - adminConfigSave:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in admin observer - adminConfigSave:".$e->getMessage(),null, 'boxx_exception.log');	
		}
	 }	
	 // Adding Notification in Admin Section
	 public function checkMessages()
	 { 
		try
		{
			Mage::getModel('core/session')->unsBoxxCredentials();
			$boxx_storeId = Mage::app()->getStore()->getStoreId();
			$boxx_customer_id = Mage::getStoreConfig('ai/ai_group/customer_id', $boxx_storeId);		//Client ID				
			$boxx_customer_key = Mage::getStoreConfig('ai/ai_group/customer_key', $boxx_storeId);		//Access Token	
			$boxx_optimise_type = Mage::getStoreConfig('ai/ai_group/optimise_type', $boxx_storeId);	//Optimise Type	
			$boxx_product_sync = Mage::getStoreConfig('ai/ai_group/product_sync', $boxx_storeId);	//product_sync
			$ai_plugin_url=Mage::helper("adminhtml")->getUrl("adminhtml/system_config/edit/section/ai/");
			$sort_by_boxx_name = Mage::getStoreConfig('ai/ai_group_rec/sort_by_boxx_name', $boxx_storeId);	//Campaign ID	
			$boxx_notifications = Mage::getSingleton('ai/notification');
			if($sort_by_boxx_name=="")
			{
				Mage::getConfig()->saveConfig('ai/ai_group_rec/sort_by_boxx_name', 'Magic', 'default', 0);
			}

			if($boxx_customer_id=='' || $boxx_customer_key=='')
			{		
				
				$boxx_notifications->addMessage("<strong style='color:red'>Important: </strong> <b>Boxx Ai - Missing Configurations</b></br>Please add Client ID & Access Token <a href='".$ai_plugin_url."'>here</a>");
				Mage::getConfig()->saveConfig('catalog/frontend/default_sort_by', 'relevance_ai', 'default', 0);			
			}
			else
			{
				$boxx_curl = curl_init();
				curl_setopt_array($boxx_curl, array(
				CURLOPT_URL => "http://app.boxx.ai/client/validate/?client_id=".$boxx_customer_id."&access_token=".$boxx_customer_key."&primary_transaction_type=".$boxx_optimise_type."",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",			
				CURLOPT_HTTPHEADER => array(
					"cache-control: no-cache",
					"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
				  ),
				));
				$boxx_response = curl_exec($boxx_curl);
				$boxx_err = curl_error($boxx_curl);
				curl_close($boxx_curl);			
				if ($boxx_err) {
				  return true;
				}
				$boxx_responseArray=json_decode($boxx_response,true);
				if($boxx_responseArray['data']['valid']!=true)
				{				
					$boxx_notifications->addMessage("<strong style='color:red'>Important: </strong> <b>Invalid Boxx Ai Configuration</b></br>Please fix <a href='".$ai_plugin_url."'>here</a>");	
				}
				else{
					//Calculating Estimated Sync Time				
					$boxx_getAllProducts = Mage::getModel('catalog/product')->getCollection();
					$boxx_ConstallProd=$boxx_getAllProducts->getSize(); //Total Products
									
					$boxx_getAllCustomers = Mage::getModel('customer/customer')->getCollection();
					$boxx_ConstallCust=$boxx_getAllCustomers->getSize();
					
					$boxx_getAllOrders = Mage::getModel('sales/order')->getCollection(); 
					$boxx_ConstallOrder=$boxx_getAllOrders->getSize();
					
					if($boxx_product_sync == "paused")
					{
						$boxx_totalTime=round(($boxx_ConstallProd+$boxx_ConstallProd+$boxx_ConstallCust+$boxx_ConstallOrder)/20000);
						if($boxx_totalTime==0)
						{
							$boxx_totalTime=1;
						}
						$boxx_notifications->addMessage("<strong class='label'>Latest Message: </strong> Data Sync is in progess, Estimated time to complete ".$boxx_totalTime." hours");
					}
					Mage::getModel('core/session')->setBoxxCredentials(
					new Varien_Object(array(
							'id' => 1,
							'customer_id' => $boxx_customer_id,
							'customer_key' => $boxx_customer_key
						))
					);
				}
				return true;	
			}
		}catch (Exception $e) {
			Mage::log("Exception in admin observer - checkmessages:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in admin observer - checkmessages:".$e->getMessage(),null, 'boxx_exception.log');	
		}
     }
}