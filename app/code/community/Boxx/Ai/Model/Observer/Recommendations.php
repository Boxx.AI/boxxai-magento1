<?php 
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * Boxx Ai PLP Recommendations Handler Model
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author	   AIBOXX Solutions Pvt Ltd
 * @copyright Copyright (c) 2016-2018 AIBOXX Solutions Pvt Ltd (https://boxx.ai)
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @user 	   Boxx Team
 */

class Boxx_Ai_Model_Observer_Recommendations extends Varien_Event_Observer
{
	  // plp recommendations
	  public function plprecommendations($observer)
	  {		

		try
		{
			$currentUrl = Mage::helper('core/url')->getCurrentUrl();
			$widget_type = "plp";
			$widget_instance = "Product List";
			$page_url = $currentUrl;
			$storeId = Mage::app()->getStore()->getStoreId();			
			if(Mage::getStoreConfig('ai/ai_group_rec/plp_show', $storeId)=="disable")
			{
				return true;
			}
			if(Mage::getBlockSingleton('catalog/product_list_toolbar')->getCurrentOrder()!="relevance_ai")
			{
				return true;
			}
			
			$pageIdentifier = Mage::app()->getFrontController()->getAction()->getFullActionName();			
			if($pageIdentifier!="catalog_category_view")
			{
				return true;
			}
			//Get Current Products List
			$productCollection = $observer->getEvent()->getCollection(); // Master Data
			
			$storeId = Mage::app()->getStore()->getStoreId();
			$customer_id = Mage::getStoreConfig('ai/ai_group/customer_id', $storeId);	//Client ID					
			$customer_key = Mage::getStoreConfig('ai/ai_group/customer_key', $storeId);	//Access Token		
			$optimise_type = Mage::getStoreConfig('ai/ai_group/optimise_type', $storeId); // Optimise Type
			// Validating the Credentials
			$curl = curl_init();
			curl_setopt_array($curl, array(
			CURLOPT_URL => "http://app.boxx.ai/client/validate/?client_id=".$customer_id."&access_token=".$customer_key."&primary_transaction_type=".$optimise_type."",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_TIMEOUT_MS =>200,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",			
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
			  ),
			));
			$response = curl_exec($curl);
			if(curl_errno($curl)){
				  Mage::log("Exception in conneciong to boxx API",null, 'boxx_exception.log');		
 				  return true;
			}
			$err = curl_error($curl);
			curl_close($curl);			
			if ($err) {
			  return true;
			}		
			$responseArray=json_decode($response,true);
		}catch (Exception $e) {
			Mage::log("Exception in recommendation observer - plprecommendations:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in recommendation observer - plprecommendations:".$e->getMessage(),null, 'boxx_exception.log');	
		}
			// Validating the Credentials - End
			if($responseArray['data']['valid'])
			{	
				try
				{
					//Getting recomendations
					$api_instance = new Boxx\Ai\Recco\Api\RecommendationApi();
					
					$CLIENT_ID = $responseArray['data']['client_id'];
					$CHANNEL_ID = $responseArray['data']['channel_id'];
					$ACCESS_TOKEN = $responseArray['data']['access_token'];
					
					$query = new Boxx\Ai\Recco\Model\RecommendationRequestDataQuery();
					
					
					// Set customer id Loggedin or Boxx Token ID
					if(Mage::getSingleton('customer/session')->isLoggedIn()) {
						$customerData = Mage::getSingleton('customer/session')->getCustomer();
						$query->setUserid($customerData->getId()); // for logged in user
					}
					else
					{
						$token=Mage::getSingleton('core/session')->getBoxxTokenSession();
						if(!$token['boxxtokenid'])
						{
							return true;
						}				
						$query->setBoxxTokenId($token['boxxtokenid']); // for not logged in user
					}			
					
					$productCollection = $observer->getEvent()->getCollection(); // Master Data
					$AllData=$productCollection->getAllIds();
					$perPagelimit=$productCollection->getPageSize()?$productCollection->getPageSize():count($productCollection->getData());
					$page_num = $productCollection->getCurPage();				
					$query->setNum($perPagelimit); // number of recomendations you want. Maximum 50.
					$query->setProductIds($AllData);
					$query->setPageNum($page_num);
					
					$request_data = new Boxx\Ai\Recco\Model\RecommendationRequestData(); 
					$request_data->setClientId($CLIENT_ID);
					$request_data->setChannelId($CHANNEL_ID);
					$request_data->setAccessToken($ACCESS_TOKEN);
					$request_data->setQuery($query);
					//Tracking Data
					$request_data->setWidgetInstance($widget_instance);
					$request_data->setWidgetType($widget_type);
					$request_data->setPageUrl($page_url);
					$recommendedProducts=array();
				}catch (Exception $e) {
					Mage::log("Exception in recommendation observer - plprecommendations:".$e->getMessage(),null, 'boxx_exception.log');
					$this->calldefaultorder($observer);		//call default position sort by when API throws error
				}catch(Error $e){
					Mage::log("Error in recommendation observer - plprecommendations:".$e->getMessage(),null, 'boxx_exception.log');
					$this->calldefaultorder($observer);	    //call default position sort by when API throws error
				}
					try {			
						$result = $api_instance->rootPost($request_data);
						foreach($result->getResult() as $key => $value){
							array_push($recommendedProducts,$value->id);
						}								
					} catch (Exception $e) {
						Mage::log("Exception when calling RecommendationApi->rootPost:".$e->getMessage(),null, 'boxx_exception.log');
						$this->calldefaultorder($observer);	    //call default position sort by when API throws error		
						return true;
					}
					
				try
				{
					if(count($recommendedProducts) > 0)
					{
						$productsToFetch=array();
						foreach($recommendedProducts as $id)
						{	
							$productsToFetch[]=$id;
						}		
					}
					else{
						return true;
					}				
					
					// Find something to update toolbar also along with data
					$productCollection = $observer->getEvent()->getCollection(); // Master Data				
					$productCollection->clear();
					$productCollection->addAttributeToFilter('entity_id', $productsToFetch);				
					$productCollection->getSelect()->order(new Zend_Db_Expr('FIELD(e.entity_id, ' . implode(",", $productsToFetch).')'));
					Mage::getBlockSingleton('catalog/product_list_toolbar')->setTotalLimit(count($productCollection->getAllIds()));	
					$productCollection->setCurPage(1);							
					$productCollection->setPageSize(count($AllData));
				}catch (Exception $e) {
					Mage::log("Exception in recommendation observer - plprecommendations:".$e->getMessage(),null, 'boxx_exception.log');		
				}catch(Error $e){
					Mage::log("Error in recommendation observer - plprecommendations:".$e->getMessage(),null, 'boxx_exception.log');	
				}
			}
			else
			{
				//return true;				
				$this->calldefaultorder($observer);    //call default position sort by when API throws error
			
			}			
	  }	

	  public function calldefaultorder($observer){
  			$toolbar = Mage::getBlockSingleton('catalog/product_list_toolbar');
			$productCollection = $observer->getEvent()->getCollection();
			$AllData = $productCollection->getAllIds();
			$productCollection->setOrder('position', $toolbar->getCurrentDirection());
			$productCollection->setCurPage($toolbar->getCurrentPage());
			$productCollection->setPageSize(count($AllData));
	  } 	  
 }   
