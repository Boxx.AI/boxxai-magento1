<?php 
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * Boxx Ai Frontend Observer Handler Model
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author	   AIBOXX Solutions Pvt Ltd
 * @copyright Copyright (c) 2016-2018 AIBOXX Solutions Pvt Ltd (https://boxx.ai)
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @user 	   Boxx Team
 */

class Boxx_Ai_Model_Observer_Frontend extends Varien_Event_Observer
{
	/************************** ALL FRONTEND INTERACTION -- STARTS *****************************/

	  // Track Newsletter subscribtion
	  public function newsletterSubscribeTrack($observer)
	  {	
	  	try
	  	{
			$boxx_subscriber = $observer->getEvent()->getSubscriber();
			$boxx_data = $boxx_subscriber->getData();
			if($boxx_data)
			{
				Mage::getModel('core/session')->setBoxxNewsletterSubscribeTrack(
					new Varien_Object(array(
						'id' => (($boxx_data['customer_id'])?$boxx_data['customer_id']:""),
						'subscriber_status' => (($boxx_data['subscriber_status'])?$boxx_data['subscriber_status']:""),
						'customer_id' => (($boxx_data['customer_id'])?$boxx_data['customer_id']:""),
						'subscriber_email' => (($boxx_data['subscriber_email'])?$boxx_data['subscriber_email']:"")
					))
				);
			}
		}catch (Exception $e) {
			Mage::log("Exception in frontend observer - newsletterSubscribeTrack:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in frontend observer - newsletterSubscribeTrack:".$e->getMessage(),null, 'boxx_exception.log');	
		}
				
	  }

	   // Track Customer Login
	  public function customerLoginTrack($observer)
	  {			
	  	try
	  	{
			$customerLoginData = $observer->getCustomer();
			if($customerLoginData)
			{
				$sessiondata = array();
				if(Mage::getModel('core/session')->getBoxxActivityTrack()){
					$sessiondata = Mage::getModel('core/session')->getBoxxActivityTrack();
				}
				
				$interactionData = array(
					'id'=>uniqid(),
					'customer_id'=>$customerLoginData->getId(),
					'interaction_id' => 'site',
					'type'=>'sign_in'
				);
				
				$sessiondata['boxxai_activity_session']['sign_in_'.uniqid()] = $interactionData; 

				Mage::getModel('core/session')->setBoxxActivityTrack($sessiondata);
				
			}	
		}catch (Exception $e) {
			Mage::log("Exception in frontend observer - customerLoginTrack:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in frontend observer - customerLoginTrack:".$e->getMessage(),null, 'boxx_exception.log');	
		}					 
	  }
	  

	    // Track Customer Register
	  public function customerRegisterTrack($observer)
	  {			
		try
		{	
			$customerLoginData = $observer->getCustomer();
			if($customerLoginData)
			{
				
				$sessiondata = array();
				if(Mage::getModel('core/session')->getBoxxActivityTrack()){
					$sessiondata = Mage::getModel('core/session')->getBoxxActivityTrack();
				}
				
				$interactionData = array(
					'id'=>uniqid(),
					'customer_id'=>$customerLoginData->getId(),
					'interaction_id' => 'site',
					'type'=>'sign_up'
				);
				
				$sessiondata['boxxai_activity_session']['sign_up_'.uniqid()] = $interactionData; 

				Mage::getModel('core/session')->setBoxxActivityTrack($sessiondata);
				
				//updating customer flag to synced as its a frontend interaction		 
				//$customerLoginData->setData('boxx_customer_flag', '1')->getResource()->saveAttribute($customerLoginData, 'boxx_customer_flag');	
			}
		}catch (Exception $e) {
			Mage::log("Exception in frontend observer - customerRegisterTrack:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in frontend observer - customerRegisterTrack:".$e->getMessage(),null, 'boxx_exception.log');	
		}
			
	  }


	  // Track Remove from Cart
	 public function pushRemovetoCart($observer){
	 	try
	 	{
			$removedItem = $observer->getEvent()->getQuoteItem();
			if($removedItem)
			{
				$productId=$removedItem->getProductId();
				if($removedItem->getProduct()->getTypeId()=="configurable")
				{
					$simpleProduct=Mage::getModel('catalog/product')->loadByAttribute('sku', $removedItem->getSku());
					$productId=$simpleProduct->getId();
				}	
				
				$sessiondata = array();
				if(Mage::getModel('core/session')->getBoxxActivityTrack()){
					$sessiondata = Mage::getModel('core/session')->getBoxxActivityTrack();
				}
				
				$interactionData = array(
					'id'=>uniqid(),
					'customer_id'=>$this->getLoggedInCustomerId(),
					'interaction_id' => $productId,
					'type'=>'RemoveFromCart'
				);
				
				$sessiondata['boxxai_activity_session']['RemoveFromCart_'.uniqid()] = $interactionData; 

				Mage::getModel('core/session')->setBoxxActivityTrack($sessiondata);
				
			}		
		}catch (Exception $e) {
			Mage::log("Exception in frontend observer - pushRemovetoCart:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in frontend observer - pushRemovetoCart:".$e->getMessage(),null, 'boxx_exception.log');	
		}
	 }
	 
	 // Track Add to Cart
	 public function pushAddtoCart($observer){
		 
	 	try
	 	{
			$productAddedToCart = Mage::getModel('catalog/product')->load(Mage::app()->getRequest()->getParam('product', 0));
			if($productAddedToCart)
			{		
				
				$productId=$productAddedToCart->getId();
				
				if($productAddedToCart->getTypeId() == "configurable")
				{
					$childProduct = Mage::getModel('catalog/product_type_configurable')->getProductByAttributes(Mage::app()->getRequest()->getParam('super_attribute'), $productAddedToCart);
					$productId=$childProduct->getId();
				}
				
				$sessiondata = array();
				if(Mage::getModel('core/session')->getBoxxActivityTrack()){
					$sessiondata = Mage::getModel('core/session')->getBoxxActivityTrack();
				}
				
				$interactionData = array(
					'id'=>uniqid(),
					'customer_id'=>$this->getLoggedInCustomerId(),
					'interaction_id' => $productId,
					'type'=>'AddToCart'
				);
				
				$sessiondata['boxxai_activity_session']['AddToCart_'.uniqid()] = $interactionData; 

				Mage::getModel('core/session')->setBoxxActivityTrack($sessiondata);
				
			}
		}catch (Exception $e) {
			Mage::log("Exception in frontend observer - pushAddtoCart:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in frontend observer - pushAddtoCart:".$e->getMessage(),null, 'boxx_exception.log');	
		}
	 }
	 
	 // Track Add to Wishlist
	 public function pushWishlisttoCart($observer){
		 
	 	try
	 	{
			 $wishListItem = $observer->getEvent()->getItem();
			 if($wishListItem)
			 {
				$sessiondata = array();
				if(Mage::getModel('core/session')->getBoxxActivityTrack()){
					$sessiondata = Mage::getModel('core/session')->getBoxxActivityTrack();
				}
				
				$interactionData = array(
					'id'=>uniqid(),
					'customer_id'=>$this->getLoggedInCustomerId(),
					'interaction_id' => $wishListItem->getProductId(),
					'type'=>'AddToWishlist'
				);
				
				$sessiondata['boxxai_activity_session']['AddToWishlist_'.uniqid()] = $interactionData; 

				Mage::getModel('core/session')->setBoxxActivityTrack($sessiondata);
			 }
 		}catch (Exception $e) {
			Mage::log("Exception in frontend observer - pushWishlisttoCart:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in frontend observer - pushWishlisttoCart:".$e->getMessage(),null, 'boxx_exception.log');	
		}
		 
	 }



	 // Track Remove from Wishlist
	 public function pushRemoveWishlisttoCart($observer){
	 	try
	 	{	 
			$wishListItemRemoved = Mage::app()->getRequest()->getParam('item');
			if($wishListItemRemoved){
				$wishListItemRemoved = Mage::getModel('wishlist/item')->load($wishListItemRemoved);
				$productId = $wishListItemRemoved->getProductId();
				
				$sessiondata = array();
				if(Mage::getModel('core/session')->getBoxxActivityTrack()){
					$sessiondata = Mage::getModel('core/session')->getBoxxActivityTrack();
				}
				
				$interactionData = array(
					'id'=>uniqid(),
					'customer_id'=>$this->getLoggedInCustomerId(),
					'interaction_id' => $productId,
					'type'=>'RemoveFromWishlist'
				);
				
				$sessiondata['boxxai_activity_session']['RemoveFromWishlist_'.uniqid()] = $interactionData; 

				Mage::getModel('core/session')->setBoxxActivityTrack($sessiondata);
				
			}
		}catch (Exception $e) {
			Mage::log("Exception in frontend observer - pushRemoveWishlisttoCart:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in frontend observer - pushRemoveWishlisttoCart:".$e->getMessage(),null, 'boxx_exception.log');	
		}		 
	 }
	 
	 // Track Checkout
	 public function pushCheckoutData($observer){	
	 	try
	 	{
			$cart = Mage::getModel('checkout/cart')->getQuote();				
			if($cart)
			{
				$cartItemsArray=array();
				foreach ($cart->getAllVisibleItems() as $item) {
					$productId=$item->getProduct()->getId();
					if($item->getProduct()->getTypeId()=="configurable")
					{
						$simpleProduct=Mage::getModel('catalog/product')->loadByAttribute('sku', $item->getSku());
						$productId=$simpleProduct->getId();
					}			
					array_push($cartItemsArray,$productId);
				}
				Mage::getModel('core/session')->setBoxxCheckoutData(
					new Varien_Object(array(
						'id' => $cart->getId(),
						'cartItems'=>$cartItemsArray,
					))
				 );	
			}
		}catch (Exception $e) {
			Mage::log("Exception in frontend observer - pushCheckoutData:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in frontend observer - pushCheckoutData:".$e->getMessage(),null, 'boxx_exception.log');	
		}
			
	 }
	 
	 // Track Order Placed
	 public function pushOrderData($observer){
	 	try
	 	{		 
			$orderDetails = $observer->getEvent()->getOrder();
			if($orderDetails)
			{
				$ProductIds = array();
				$order = Mage::getModel('sales/order')->load($orderDetails->getId());
				if( $orderDetails->getId() ) {				
					foreach( $order->getAllVisibleItems() as $item ) {
						$productId=$item->getProduct()->getId();
						if($item->getProduct()->getTypeId()=="configurable")
						{
							$simpleProduct=Mage::getModel('catalog/product')->loadByAttribute('sku', $item->getSku());
							$productId=$simpleProduct->getId();
						}	
						$this->productQtyUpdate($productId);
					}        
				}
			}
		}catch (Exception $e) {
			Mage::log("Exception in frontend observer - pushOrderData:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in frontend observer - pushOrderData:".$e->getMessage(),null, 'boxx_exception.log');	
		}			
	 }
	 
	 /************************** ALL FRONTEND INTERACTION -- ENDS *****************************/
	 
	 // Product Qty Change
     public function productQtyUpdate($productId){
     	try
     	{
     		Mage::helper('ai')->markProductsForUpdate($productId);	     	
		}catch (Exception $e) {
			Mage::log("Exception in frontend observer - productQtyUpdate:".$e->getMessage(),null, 'boxx_exception.log');		
		}catch(Error $e){
			Mage::log("Error in frontend observer - productQtyUpdate:".$e->getMessage(),null, 'boxx_exception.log');	
		}
     }
	 
	public function getLoggedInCustomerId()
	{
		$boxxcustomer_id="";
		// Get Customer Id
		if(Mage::getSingleton('customer/session')->isLoggedIn()) {
			$boxxcustomerData = Mage::getSingleton('customer/session')->getCustomer();
			$boxxcustomer_id=$boxxcustomerData->getId();
		}
		return $boxxcustomer_id;
	}

}