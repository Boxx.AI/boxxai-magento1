<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * Boxx Ai Lib Autoloader
 *
 * @category   Boxx
 * @package    Boxx_Ai
 * @author	   AIBOXX Solutions Pvt Ltd
 * @copyright Copyright (c) 2016-2018 AIBOXX Solutions Pvt Ltd (https://boxx.ai)
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @user 	   Boxx Team
*/

class Boxx_Ai_Model_Autoloader
{
    protected static $_registered = false;

    /**
     * Add autoloader
     *
     * @param Varien_Event_Observer $observer
     * @return \Boxx_Ai_Model_Autoloader
     */
    public function addAutoloader(Varien_Event_Observer $observer)
    {
        try
        {
            if (self::$_registered) {
                return $this;
            }
            spl_autoload_register(array($this, 'autoload'), false, true);

            self::$_registered = true;
            return $this;
        }catch (Exception $e) {
            Mage::log("Exception in model addAutoloader:".$e->getMessage(),null, 'boxx_exception.log');        
        }catch(Error $e){
            Mage::log("Error in model addAutoloader:".$e->getMessage(),null, 'boxx_exception.log');    
        }
    }

    /**
     * Autoload
     *
     * @param string $class
     */
    public function autoload($class)
    {
        try
        {
            $boxxclassFile = str_replace('\\', DS, $class) . '.php';
            // Actually check if file exists in include path, do nothing otherwise
            if (stream_resolve_include_path($boxxclassFile) !== false) {
                include $boxxclassFile;
            }
        }catch (Exception $e) {
            Mage::log("Exception in model autoload:".$e->getMessage(),null, 'boxx_exception.log');        
        }catch(Error $e){
            Mage::log("Error in model autoload:".$e->getMessage(),null, 'boxx_exception.log');    
        }
    }
}