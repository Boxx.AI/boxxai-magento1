<?php
/**
 * UploadProduct
 *
 * PHP version 5
 *
 * @category Class
 * @package  Boxx\Ai\Upload
 * @author   Swaagger Codegen team
 * @link     https://github.com/boxx-api/boxx-codegen
 */

/**
 * Upload and Delete Data API
 *
 * Boxx.ai's customer, product and interaction data ingestion sdk
 *
 * OpenAPI spec version: 1.4.0
 * Contact: aida@boxx.ai
 * Generated by: https://github.com/boxx-api/boxx-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the boxx code generator program.
 * https://github.com/boxx-api/boxx-codegen
 * Do not edit the class manually.
 */

namespace Boxx\Ai\Upload\Model;

use \ArrayAccess;

/**
 * UploadProduct Class Doc Comment
 *
 * @category    Class
 * @description Upload data product schema
 * @package     Boxx\Ai\Upload
 * @author      boxx Codegen team
 * @link        https://github.com/boxx-api/boxx-codegen
 */
class UploadProduct implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $boxxModelName = 'upload_product';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $boxxTypes = [
        'productid' => 'string',
        'active' => 'bool',
        'start_date' => 'string',
        'end_date' => 'string',
        'location' => 'string[]',
        'properties' => 'map[string,object]'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $boxxFormats = [
        'productid' => null,
        'active' => null,
        'start_date' => null,
        'end_date' => null,
        'location' => null,
        'properties' => null
    ];

    public static function boxxTypes()
    {
        return self::$boxxTypes;
    }

    public static function boxxFormats()
    {
        return self::$boxxFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'productid' => 'productid',
        'active' => 'active',
        'start_date' => 'start_date',
        'end_date' => 'end_date',
        'location' => 'location',
        'properties' => 'properties'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'productid' => 'setProductid',
        'active' => 'setActive',
        'start_date' => 'setStartDate',
        'end_date' => 'setEndDate',
        'location' => 'setLocation',
        'properties' => 'setProperties'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'productid' => 'getProductid',
        'active' => 'getActive',
        'start_date' => 'getStartDate',
        'end_date' => 'getEndDate',
        'location' => 'getLocation',
        'properties' => 'getProperties'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['productid'] = isset($data['productid']) ? $data['productid'] : null;
        $this->container['active'] = isset($data['active']) ? $data['active'] : false;
        $this->container['start_date'] = isset($data['start_date']) ? $data['start_date'] : null;
        $this->container['end_date'] = isset($data['end_date']) ? $data['end_date'] : null;
        $this->container['location'] = isset($data['location']) ? $data['location'] : null;
        $this->container['properties'] = isset($data['properties']) ? $data['properties'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        if ($this->container['productid'] === null) {
            $invalid_properties[] = "'productid' can't be null";
        }
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        if ($this->container['productid'] === null) {
            return false;
        }
        return true;
    }


    /**
     * Gets productid
     * @return string
     */
    public function getProductid()
    {
        return $this->container['productid'];
    }

    /**
     * Sets productid
     * @param string $productid
     * @return $this
     */
    public function setProductid($productid)
    {
        $this->container['productid'] = $productid;

        return $this;
    }

    /**
     * Gets active
     * @return bool
     */
    public function getActive()
    {
        return $this->container['active'];
    }

    /**
     * Sets active
     * @param bool $active
     * @return $this
     */
    public function setActive($active)
    {
        $this->container['active'] = $active;

        return $this;
    }

    /**
     * Gets start_date
     * @return string
     */
    public function getStartDate()
    {
        return $this->container['start_date'];
    }

    /**
     * Sets start_date
     * @param string $start_date
     * @return $this
     */
    public function setStartDate($start_date)
    {
        $this->container['start_date'] = $start_date;

        return $this;
    }

    /**
     * Gets end_date
     * @return string
     */
    public function getEndDate()
    {
        return $this->container['end_date'];
    }

    /**
     * Sets end_date
     * @param string $end_date
     * @return $this
     */
    public function setEndDate($end_date)
    {
        $this->container['end_date'] = $end_date;

        return $this;
    }

    /**
     * Gets location
     * @return string[]
     */
    public function getLocation()
    {
        return $this->container['location'];
    }

    /**
     * Sets location
     * @param string[] $location
     * @return $this
     */
    public function setLocation($location)
    {
        $this->container['location'] = $location;

        return $this;
    }

    /**
     * Gets properties
     * @return map[string,object]
     */
    public function getProperties()
    {
        return $this->container['properties'];
    }

    /**
     * Sets properties
     * @param map[string,object] $properties
     * @return $this
     */
    public function setProperties($properties)
    {
        $this->container['properties'] = $properties;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Boxx\Ai\Upload\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Boxx\Ai\Upload\ObjectSerializer::sanitizeForSerialization($this));
    }
}


